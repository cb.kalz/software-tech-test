# notes

**BUILD_ALL.sh** : runs tests first and then builds and runs the Application

Access Application at [**localhost:3000**](http://localhost:3000/)

```bash
Check README.md in /drone_backend

I have included comments and notes about my thought process/decision making everywhere!

chmod +x BUILD_ALL.sh
./BUILD_ALL.sh

# or just run the cmd as is
docker-compose up --build


# Can also simply

cd drone_backend/application/functional
go run main.go

# Check drone_backend/API.md file to view all commands to interact with the API
```

# Software-Tech-Test

One of DroneShield's missions is to provide the best Counterdrone defence in an emerging industry.
This challenge involves building a simulator for Counter UAS involving the detection of drones.

The provided code simulates two microservices that interface with each through redis publish and subscribe.

One microservice that publishes to redis the coordinates of the drone and another microservice that subscribes
to the event and pushes to a websocket (similar to the sample).

Currently, the frontend provides:

- A connection to the websocket with the received data logged to the console
- A map
- An icon of a drone on the map

In a functioning app, the drone icon will move on the map depending on the location provided in the websocket message.
Additionally, there should be a selectable simulated flying pattern of the drone such as "Figure 8", "Circle" or "Zigzag".
More advanced and intelligent flight paths will be given additional brownie points.
The flying pattern can be changed via HTTP request or direct command through websocket.

Task:
Review the provided frontend and backend code and give comments on what would need to be done to make it production ready including testing.
Feel free to write code to complete the implementation of the app instead of only providing comments.

## Instructions to run the sample

```
# Bring it up
docker compose up --build

# Websocket endpoint, use your favourite WS client
ws://localhost:8080
```
