#!/bin/bash

echo -e "Building Go Backend library and running tests"

docker-compose build drone_backend_tests
docker-compose up --exit-code-from drone_backend_tests drone_backend_tests
BACKEND_TESTS_EXIT_CODE=$?

echo -e "Transpiling Frontend and running tests"

docker-compose build drone_frontend_tests
docker-compose up --exit-code-from drone_frontend_tests drone_frontend_tests
FRONTEND_TESTS_EXIT_CODE=$?

echo -e "Building Services in Release Mode and Launching Application"

if [ $BACKEND_TESTS_EXIT_CODE -eq 0 ] && [ $FRONTEND_TESTS_EXIT_CODE -eq 0 ]; then
    echo "Both test suites passed. Starting services..."
    docker-compose up -d drone_publisher drone_subscriber drone_frontend
else
    echo "One or more test suites failed."
    exit 1
fi


echo -e "Application Running at http://localhost/3000/"