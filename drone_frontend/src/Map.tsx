import { FC, useState, useRef } from "react";

import L from "leaflet";
import { TileLayer } from "react-leaflet";
import { Marker, MarkerLayer } from "react-leaflet-marker";

import GotoButton from "./button/goto";
import DroneMarker from "./drone/DroneMarker";
import FollowButton from "./button/follow";
import DistanceButton from "./button/distance";
import ZigZagMovementButton from "./drone/zigzag";
import NavigateToPointButton from "./drone/navigateto";
import CircularMovementButton from "./drone/circular";
import DirectionalMovementButton from "./drone/directional";

import { ChangeView } from "./drone/view";
import { SimpleButton } from "./ui/simple";
import { useMapActions } from "./map/actions";
import { MapProps, StyledMap } from "./map/props";
import { TrailDrone, ToggleTrailButton, ClearTrailButton } from "./drone/trail";

import "./Map.css";

const Map: FC<MapProps> = ({ droneCoords, sendCommand }) => {
  /* State */
  const mapRef = useRef<L.Map | null>(null);
  const [showTrail, setShowTrail] = useState(false);
  const [isMeasuring, setIsMeasuring] = useState(false);
  const [distanceResult, setDistanceResult] = useState<string>("");

  /* Referential State */
  const {
    goToDrone,
    followDrone,
    currentCenter,
    toggleFollowDrone,
    followDroneShouldUpdate,
  } = useMapActions(mapRef, droneCoords);

  const [trailKey, setTrailKey] = useState<number>(0);

  const clearTrail = () => {
    setTrailKey((prevKey) => prevKey + 1); // Increment the key to force remount of TrailDrone
  };

  return (
    <StyledMap
      ref={mapRef}
      center={currentCenter}
      zoom={14}
      scrollWheelZoom={false}
    >
      <TileLayer
        attribution=""
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <MarkerLayer>
        <Marker position={[droneCoords.latitude, droneCoords.longitude]}>
          <DroneMarker />
        </Marker>
      </MarkerLayer>

      <TrailDrone
        key={trailKey}
        droneCoords={droneCoords}
        showTrail={showTrail}
      />
      <ClearTrailButton clearTrail={clearTrail} />

      {/* <TrailDrone droneCoords={droneCoords} showTrail={showTrail} />
      <ClearTrailButton clearTrail={() => setShowTrail(false)} /> */}
      <ToggleTrailButton
        toggleShowTrail={() => setShowTrail(!showTrail)}
        showTrail={showTrail}
      />

      <NavigateToPointButton sendCommand={sendCommand} />

      <DirectionalMovementButton direction="North" sendCommand={sendCommand} />

      {distanceResult && <SimpleButton text={distanceResult} />}
      <DistanceButton
        isMeasuring={isMeasuring}
        setIsMeasuring={setIsMeasuring}
        setDistanceResult={setDistanceResult}
      />

      <CircularMovementButton sendCommand={sendCommand} />

      <ZigZagMovementButton sendCommand={sendCommand} />

      <GotoButton goToDrone={goToDrone} />
      {followDrone && followDroneShouldUpdate && (
        <ChangeView center={currentCenter} zoom={14} />
      )}

      <FollowButton
        isFollowing={followDrone}
        toggleFollow={toggleFollowDrone}
      />
    </StyledMap>
  );
};

export default Map;
