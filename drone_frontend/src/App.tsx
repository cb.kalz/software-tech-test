import Map from "./Map";

import { useWebSocketClient } from "./webclient/wsocket";

const App = () => {
  const websocketUrl =
    process.env.REACT_APP_WEBSOCKET_URL || "ws://localhost:8081/";
  console.log(`Connecting to Backend at ${websocketUrl}`);

  const { sendCommand, dronePosition } = useWebSocketClient(websocketUrl);

  return <Map droneCoords={dronePosition} sendCommand={sendCommand} />;
};

export default App;
