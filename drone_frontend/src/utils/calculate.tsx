import L from "leaflet";

type Position = {
  latitude: number;
  longitude: number;
};

export const shouldUpdateCenter = (
  map: L.Map | null,
  dronePosition: Position,
  thresholdPercentage: number = 35
): boolean => {
  if (!map) return false;

  const mapCenter = map.getCenter();
  const droneLatLng = new L.LatLng(
    dronePosition.latitude,
    dronePosition.longitude
  );

  const mapHeight = map.getSize().y;
  const mapCenterPoint = map.latLngToContainerPoint(mapCenter);
  const dronePoint = map.latLngToContainerPoint(droneLatLng);

  const verticalDistance = Math.abs(dronePoint.y - mapCenterPoint.y);
  const percentageOfHeight = (verticalDistance / mapHeight) * 100;

  return percentageOfHeight > thresholdPercentage;
};

export const getCurrentCenter = (
  map: L.Map | null,
  dronePosition: Position,
  followDrone: boolean,
  shouldUpdate: boolean
): [number, number] => {
  if (followDrone && shouldUpdate) {
    return [dronePosition.latitude, dronePosition.longitude];
  }
  const currentCenter = map?.getCenter();
  return currentCenter
    ? [currentCenter.lat, currentCenter.lng]
    : [-33.946765, 151.1796423];
};
