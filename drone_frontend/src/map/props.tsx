import { MapContainer } from "react-leaflet";
import styled from "@emotion/styled";
import { GeoPosition } from "../types/position";

export type MapProps = {
  droneCoords: GeoPosition;
  sendCommand: (command: any) => void;
};

export const StyledMap = styled(MapContainer)`
  height: calc(100vh);
  position: relative;
`;
