// useMapActions.js
import { useState, useCallback, RefObject } from "react";
import { isValidCoordinate } from "../utils/validity";
import { shouldUpdateCenter, getCurrentCenter } from "../utils/calculate";
import { GeoPosition } from "../types/position";

export const useMapActions = (
  mapRef: RefObject<L.Map>,
  dronePosition: GeoPosition
) => {
  const [followDrone, setFollowDrone] = useState(false);

  const toggleFollowDrone = useCallback(() => {
    setFollowDrone((prev) => !prev);
  }, []);

  const goToDrone = useCallback(() => {
    if (
      mapRef.current &&
      isValidCoordinate(dronePosition.latitude, dronePosition.longitude)
    ) {
      mapRef.current.flyTo(
        [dronePosition.latitude, dronePosition.longitude],
        14
      );
    }
  }, [mapRef, dronePosition]);

  const currentCenter = getCurrentCenter(
    mapRef.current,
    dronePosition,
    followDrone,
    shouldUpdateCenter(mapRef.current, dronePosition)
  );

  const followDroneShouldUpdate = shouldUpdateCenter(mapRef.current, {
    latitude: dronePosition.latitude,
    longitude: dronePosition.longitude,
  });

  return {
    followDroneShouldUpdate,
    followDrone,
    toggleFollowDrone,
    goToDrone,
    currentCenter,
  };
};
