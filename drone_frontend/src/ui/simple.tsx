import React from "react";

type SimpleButtonProps = {
  text: string;
  extraStyles?: React.CSSProperties;
};

export const SimpleButton: React.FC<SimpleButtonProps> = ({
  text,
  extraStyles,
}) => {
  return (
    <button
      style={{
        position: "absolute",
        zIndex: 1000,
        backgroundColor: "white",
        padding: "10px",
        top: "6rem",
        left: "1rem",
        ...extraStyles,
      }}
    >
      {text}
    </button>
  );
};
