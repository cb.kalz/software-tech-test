import { GeoPosition } from "../types/position";
import { Direction } from "@mui/material";

export type CircularPatternReq = {
  action: "changePattern";
  pattern: "Circular";
  params: { center: GeoPosition };
};

export type ZigZagPatternReq = {
  action: "changePattern";
  pattern: "ZigZag";
  params: { target: GeoPosition; angle: number; increment: number };
};

export type StraightPatternReq = {
  action: "changePattern";
  pattern: "Straight";
  params: { direction: string };
};

export type GoToReq = {
  action: "goTo";
  params: { target: GeoPosition };
};
