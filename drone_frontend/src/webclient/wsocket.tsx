import { useState, useEffect, useCallback, useRef } from "react";

export interface WebSocketClient {
  sendCommand: (command: any) => void;
  dronePosition: { latitude: number; longitude: number };
}

export const useWebSocketClient = (url: string): WebSocketClient => {
  const [dronePosition, setDronePosition] = useState<{
    latitude: number;
    longitude: number;
  }>({ latitude: 0, longitude: 0 });
  const websocketRef = useRef<WebSocket | null>(null);
  const retryCountRef = useRef<number>(0);

  const connectWebSocket = useCallback(
    (attempt: number) => {
      const retryDelay = Math.min(8000, Math.pow(2, attempt) * 100);

      setTimeout(() => {
        if (
          websocketRef.current &&
          websocketRef.current.readyState === WebSocket.OPEN
        ) {
          console.log("WebSocket already connected.");
          return;
        }

        const ws = new WebSocket(url);

        ws.onopen = () => {
          console.log("WebSocket connected");
          retryCountRef.current = 0;
        };

        ws.onmessage = (event) => {
          const data = JSON.parse(event.data);
          console.log("Received Data: ", data);
          if (data.latitude && data.longitude) {
            setDronePosition({
              latitude: data.latitude,
              longitude: data.longitude,
            });
          } else if (data.type === "status") {
            console.log("Status Update: ", data.message);
          }
        };

        ws.onclose = () => {
          console.log(
            "WebSocket disconnected, attempting to reconnect with Backoff....."
          );
          connectWebSocket((retryCountRef.current += 1));
        };

        ws.onerror = (error) => {
          console.error("WebSocket error:", error);
          ws.close();
        };

        websocketRef.current = ws;
      }, retryDelay);
    },
    [url]
  );

  useEffect(() => {
    connectWebSocket(retryCountRef.current);

    return () => {
      websocketRef.current?.close();
    };
  }, [connectWebSocket]);

  const sendCommand = useCallback((command: any) => {
    console.log("Sending command:", command);
    if (websocketRef.current?.readyState === WebSocket.OPEN) {
      console.log("WebSocket is open, sending command.");
      websocketRef.current.send(JSON.stringify(command));
    } else {
      console.error("WebSocket is not connected.");
    }
  }, []);

  return { sendCommand, dronePosition };
};
