import { useState, useEffect, useCallback, useRef } from "react";
import { WebSocketClient } from "./wsocket";

/*

Currently the Frontend implements HTTP as a fallback for WS which is a common pattern

However, for Subscribing to the Current Position HTTP would not be ideal as the latency to connect through the ISP over Fiber alone is typically 50-150ms and we can double/triple that assuming TLS and no HTTp/2.

A solution for this would be buffering the coordinates and sending them in buffers of 10+ or 3s+ increments.

For Req/Response kind of situations HTTP is ideal.

@Usage

const { sendCommand, dronePosition } = useWebSocketHttpFallbackClient(
    "ws://localhost:8081/",
    "http://localhost:8080"
  );


*/
export const useWebSocketHttpFallbackClient = (
  websocketUrl: string,
  httpUrl: string
): WebSocketClient => {
  const [dronePosition, setDronePosition] = useState<{
    latitude: number;
    longitude: number;
  }>({ latitude: 0, longitude: 0 });
  const websocketRef = useRef<WebSocket | null>(null);
  const retryCountRef = useRef<number>(0);

  // Function to send command via HTTP as a fallback
  const sendCommandViaHttp = useCallback(
    async (command: any) => {
      console.log("Sending command via HTTP:", command);
      try {
        const response = await fetch(`${httpUrl}/api`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(command),
        });
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        console.log("Command sent via HTTP successfully.");
      } catch (error) {
        console.error("Failed to send command via HTTP:", error);
      }
    },
    [httpUrl]
  );

  const connectWebSocket = useCallback(
    (attempt: number) => {
      const retryDelay = Math.min(8000, Math.pow(2, attempt) * 100);

      setTimeout(() => {
        if (
          websocketRef.current &&
          websocketRef.current.readyState === WebSocket.OPEN
        ) {
          console.log("WebSocket already connected.");
          return;
        }

        const ws = new WebSocket(websocketUrl);

        ws.onopen = () => {
          console.log("WebSocket connected");
          retryCountRef.current = 0;
        };

        ws.onmessage = (event) => {
          const data = JSON.parse(event.data);
          console.log("Received Data: ", data);
          if (data.latitude && data.longitude) {
            setDronePosition({
              latitude: data.latitude,
              longitude: data.longitude,
            });
          }
        };

        ws.onclose = () => {
          console.log(
            "WebSocket disconnected, attempting to reconnect with Backoff....."
          );
          connectWebSocket((retryCountRef.current += 1));
        };

        ws.onerror = (error) => {
          console.error("WebSocket error:", error);
          ws.close();
        };

        websocketRef.current = ws;
      }, retryDelay);
    },
    [websocketUrl]
  );

  useEffect(() => {
    connectWebSocket(retryCountRef.current);

    return () => {
      websocketRef.current?.close();
    };
  }, [connectWebSocket]);

  const sendCommand = useCallback(
    (command: any) => {
      console.log("Attempting to send command:", command);
      if (websocketRef.current?.readyState === WebSocket.OPEN) {
        console.log("WebSocket is open, sending command.");
        websocketRef.current.send(JSON.stringify(command));
      } else {
        console.error("WebSocket is not connected, falling back to HTTP.");
        sendCommandViaHttp(command);
      }
    },
    [sendCommandViaHttp]
  );

  return { sendCommand, dronePosition };
};
