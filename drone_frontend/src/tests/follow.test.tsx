import { render, screen, fireEvent } from "@testing-library/react";
import FollowButton from "../button/follow";
import GotoButton from "../button/goto";
/* Confirm Follow button toggle works */
test("it toggles follow state", () => {
  const mockToggle = vi.fn();
  render(<FollowButton isFollowing={false} toggleFollow={mockToggle} />);
  const button = screen.getByText(/Follow Drone/i);
  fireEvent.click(button);
  expect(mockToggle).toHaveBeenCalled();
});

test("it triggers go to drone action", () => {
  const mockGoToDrone = vi.fn();
  render(<GotoButton goToDrone={mockGoToDrone} />);
  const button = screen.getByText(/Go to Drone/i);
  fireEvent.click(button);
  expect(mockGoToDrone).toHaveBeenCalled();
});
