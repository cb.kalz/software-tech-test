import { render } from "@testing-library/react";
import { MapContainer } from "react-leaflet";
import styled from "@emotion/styled";

const StyledMap = styled(MapContainer)`
  height: calc(100vh);
  position: relative;
`;

describe("StyledMap Component", () => {
  const validCenter: [number, number] = [51.505, -0.09]; // Explicitly typed as a tuple
  const invalidCenter: [number, number] = [1000, 1000]; // Invalid latitude and longitude

  it("renders correctly with valid center and zoom", () => {
    const { getByText } = render(
      <StyledMap center={validCenter} zoom={13} scrollWheelZoom={false}>
        {/* children components */}
      </StyledMap>
    );
    // Assertions to check if it's rendering properly
  });

  it("renders correctly with invalid center", () => {
    const { getByText } = render(
      <StyledMap center={invalidCenter} zoom={13} scrollWheelZoom={false}>
        {/* children components */}
      </StyledMap>
    );
    // Assertions to handle the scenario with invalid center
  });

  // Add more test cases as needed
});
