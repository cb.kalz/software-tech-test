import { render, fireEvent } from "@testing-library/react";
import Map from "../Map"; // Adjust the import path as necessary
import "@testing-library/jest-dom";

const mockSendCommand = vi.fn();

describe("Map Component", () => {
  it("renders correctly", () => {
    const { getByText } = render(
      <Map
        droneCoords={{ latitude: 0, longitude: 0 }}
        sendCommand={mockSendCommand}
      />
    );
    expect(getByText(/Follow Drone/i)).toBeInTheDocument();
    expect(getByText(/Go to Drone/i)).toBeInTheDocument();
  });

  it("toggles follow drone on button click", () => {
    const { getByText } = render(
      <Map
        droneCoords={{ latitude: 0, longitude: 0 }}
        sendCommand={mockSendCommand}
      />
    );
    const followButton = getByText(/Follow Drone/i);
    fireEvent.click(followButton);
  });
});

const validPositions = [
  { lat: 0, lng: 0 },
  { lat: 90, lng: 180 },
  { lat: -90, lng: -180 },
];

describe.each(validPositions)(
  "Map Component with Valid Positions",
  (position) => {
    it(`renders correctly with latitude: ${position.lat}, longitude: ${position.lng}`, () => {
      const { getByText } = render(
        <Map
          droneCoords={{ latitude: position.lat, longitude: position.lng }}
          sendCommand={mockSendCommand}
        />
      );
      expect(getByText(/Follow Drone/i)).toBeInTheDocument();
      expect(getByText(/Go to Drone/i)).toBeInTheDocument();
    });
  }
);

/*

invalidPositions = [{ lat: 100, lng: 200 }];

Currently when Invalid Coords - they are set to the Edge

33.946765 151.17964229999998 

*/
