// ButtonStyles.ts
import { CSSProperties } from "react";

export const bottomLeftButtonStyle: CSSProperties = {
  position: "absolute",
  left: "10px",
  zIndex: 1000,
};
