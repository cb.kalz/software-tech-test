import React from "react";

type GotoButtonProps = {
  goToDrone: () => void;
};

const GotoButton: React.FC<GotoButtonProps> = ({ goToDrone }) => {
  return (
    <button
      onClick={goToDrone}
      style={{
        position: "absolute",
        bottom: "4rem",
        left: "1rem",
        zIndex: 1000,
      }}
    >
      Go to Drone
    </button>
  );
};

export default GotoButton;
