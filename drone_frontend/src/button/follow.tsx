import React from "react";

type FollowButtonProps = {
  isFollowing: boolean;
  toggleFollow: () => void;
};

const FollowButton: React.FC<FollowButtonProps> = ({
  isFollowing,
  toggleFollow,
}) => {
  return (
    <button
      onClick={toggleFollow}
      style={{
        position: "absolute",
        bottom: "2rem",
        left: "1rem",
        zIndex: 1000,
      }}
    >
      {isFollowing ? "Unfollow Drone" : "Follow Drone"}
    </button>
  );
};

export default FollowButton;
