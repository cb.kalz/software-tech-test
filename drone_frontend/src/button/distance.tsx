import React, { useState } from "react";
import { useMapEvents, CircleMarker } from "react-leaflet";
import L from "leaflet";
import "./button.css";

export interface DistanceMeasureResult {
  point1: L.LatLng;
  point2: L.LatLng;
  distance: number;
}

interface Props {
  isMeasuring: boolean;
  setIsMeasuring: React.Dispatch<React.SetStateAction<boolean>>;
  setDistanceResult: React.Dispatch<React.SetStateAction<string>>;
}

const DistanceButton: React.FC<Props> = ({
  isMeasuring,
  setIsMeasuring,
  setDistanceResult,
}) => {
  const [markers, setMarkers] = useState<L.LatLng[]>([]);

  useMapEvents({
    click: (e) => {
      if (!isMeasuring) return;

      const newMarkers = [...markers, e.latlng];
      setMarkers(newMarkers);

      if (newMarkers.length === 2) {
        const distance = newMarkers[0].distanceTo(newMarkers[1]);
        setDistanceResult(`${distance.toFixed(2)} meters`);
      }
    },
  });

  const toggleMeasuring = () => {
    if (!isMeasuring && markers.length > 0) {
      setMarkers([]);
      setDistanceResult("");
    }

    setIsMeasuring(!isMeasuring);
  };

  return (
    <>
      {markers.map((point, index) => (
        <CircleMarker
          key={index}
          center={point}
          radius={5}
          fillColor="red"
          color="red"
        />
      ))}
      <button
        onClick={toggleMeasuring}
        style={{
          position: "absolute",
          bottom: "6rem",
          left: "1rem",
          zIndex: 1000,
          cursor: isMeasuring ? "crosshair" : "pointer",
        }}
      >
        {isMeasuring ? "Cancel Measuring" : "Start Measuring"}
      </button>
    </>
  );
};

export default DistanceButton;
