// BottomLeftButtons.tsx
import React, { FC, ReactNode } from "react";
import { bottomLeftButtonStyle } from "../styles/actionbtn";

type BottomLeftButtonsProps = {
  buttons: ReactNode[];
};

export const BottomLeftButtons: FC<BottomLeftButtonsProps> = ({ buttons }) => {
  return (
    <>
      {buttons.map((button, index) => (
        <div
          key={index}
          style={{
            ...bottomLeftButtonStyle,
            bottom: `${160 - index * 40}px`,
          }}
        >
          {button}
        </div>
      ))}
    </>
  );
};
