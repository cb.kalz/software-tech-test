export type GeoPosition = {
  latitude: number;
  longitude: number;
};

export type ReactMarketPosition = [number, number];

export enum Direction {
  NORTH = 1,
  SOUTH,
  WEST,
  EAST,
}
