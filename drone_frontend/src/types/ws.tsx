import { GeoPosition } from "./position";

export type DroneCommand = {
  action: string;
  pattern: string;
};

// Define types for the command to change the drone's movement pattern
export interface Command {
  action: string;
  pattern: string;
  params?: CircularParams; // Optional params for the pattern, if needed
}

export interface CircularParams {
  radius: number;
  center: GeoPosition;
}
