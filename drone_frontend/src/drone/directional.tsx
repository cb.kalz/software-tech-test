import { FC } from "react";
export type DirectionalProps = {
  direction: string;
  sendCommand: (command: any) => void;
};

const DirectionalMovementButton: FC<DirectionalProps> = ({
  direction,
  sendCommand,
}) => {
  const handleClick = () => {
    sendCommand({
      action: "changePattern",
      pattern: "Straight",
      params: { direction },
    });
  };

  const getButtonHeight = (direction: string): string => {
    switch (direction) {
      case "North":
        return "16rem";
      case "South":
        return "18rem";
      case "East":
        return "20rem";
      case "West":
        return "22rem";
      default:
        return "16rem";
    }
  };

  return (
    <button
      style={{
        position: "absolute",
        bottom: getButtonHeight(direction),
        left: "1rem",
        zIndex: 1000,
      }}
      onClick={handleClick}
    >
      Move {direction}
    </button>
  );
};

export default DirectionalMovementButton;
