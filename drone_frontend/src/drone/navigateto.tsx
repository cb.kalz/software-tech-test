// GoToButton.js
import { FC, useState } from "react";
import { useMap } from "react-leaflet";

export type DirectNavProps = {
  sendCommand: (command: any) => void;
};

const NavigateToPointButton: FC<DirectNavProps> = ({ sendCommand }) => {
  const [isSelecting, setIsSelecting] = useState(false);
  const map = useMap();

  const handleClick = () => {
    if (!isSelecting) {
      map.getContainer().style.cursor = "crosshair";
      setIsSelecting(true);
      map.once("click", (e) => {
        map.getContainer().style.cursor = "";
        const { lat, lng } = e.latlng;
        sendCommand({
          action: "goTo",
          params: { target: { latitude: lat, longitude: lng } },
        });
        setIsSelecting(false);
      });
    } else {
      map.getContainer().style.cursor = "";
      setIsSelecting(false);
    }
  };

  return (
    <button
      style={{
        position: "absolute",
        bottom: "18rem",
        left: "1rem",
        zIndex: 1000,
      }}
      onClick={handleClick}
    >
      {isSelecting ? "Cancel" : "Navigate to Point"}
    </button>
  );
};

export default NavigateToPointButton;
