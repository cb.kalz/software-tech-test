import { FC, useEffect } from "react";
import { useMap } from "react-leaflet";

export type ChangeViewProps = {
  center: [number, number];
  zoom: number;
};

export const ChangeView: FC<ChangeViewProps> = ({
  center,
  zoom,
}: ChangeViewProps) => {
  const map = useMap();
  useEffect(() => {
    map.flyTo(center, zoom);
  }, [center, zoom, map]);
  return null;
};
