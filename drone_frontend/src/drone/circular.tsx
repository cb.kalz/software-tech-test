import React, { useState } from "react";
import { useMap } from "react-leaflet";

type CircularMovementButtonProps = {
  sendCommand: (command: any) => void;
};

const CircularMovementButton: React.FC<CircularMovementButtonProps> = ({
  sendCommand,
}) => {
  const [isSelecting, setIsSelecting] = useState(false);
  const map = useMap();

  const handleClick = () => {
    if (!isSelecting) {
      map.getContainer().style.cursor = "crosshair";
      setIsSelecting(true);
      map.once("click", (e) => {
        map.getContainer().style.cursor = "";
        const { lat, lng } = e.latlng;
        sendCommand({
          action: "changePattern",
          pattern: "Circular",
          params: { center: { latitude: lat, longitude: lng } },
        });
        setIsSelecting(false);
      });
    } else {
      map.getContainer().style.cursor = "";
      setIsSelecting(false);
    }
  };

  return (
    <button
      style={{
        position: "absolute",
        bottom: "12rem",
        left: "1rem",
        zIndex: 1000,
      }}
      onClick={handleClick}
    >
      {isSelecting ? "Cancel" : "Change to Circular Movement"}
    </button>
  );
};

export default CircularMovementButton;
