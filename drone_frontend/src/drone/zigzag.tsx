import React, { useState } from "react";
import { useMap } from "react-leaflet";

type ZigZagMovementButtonProps = {
  sendCommand: (command: any) => void;
};

const ZigZagMovementButton: React.FC<ZigZagMovementButtonProps> = ({
  sendCommand,
}) => {
  const [isSelecting, setIsSelecting] = useState(false);
  const map = useMap();

  const handleClick = () => {
    if (!isSelecting) {
      map.getContainer().style.cursor = "crosshair";
      setIsSelecting(true);
      map.once("click", (e) => {
        map.getContainer().style.cursor = "";
        const { lat, lng } = e.latlng;
        sendCommand({
          action: "changePattern",
          pattern: "ZigZag",
          params: { target: { latitude: lat, longitude: lng }, angle: 45.0 },
        });
        setIsSelecting(false);
      });
    } else {
      map.getContainer().style.cursor = "";
      setIsSelecting(false);
    }
  };

  return (
    <button
      style={{
        position: "absolute",
        bottom: "14rem",
        left: "1rem",
        zIndex: 1000,
      }}
      onClick={handleClick}
    >
      {isSelecting ? "Cancel" : "Change to ZigZag Movement"}
    </button>
  );
};

export default ZigZagMovementButton;
