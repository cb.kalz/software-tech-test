// TrailComponent.tsx
import React, { FC, useEffect, useState } from "react";
import { CircleMarker } from "react-leaflet";
import { GeoPosition } from "../types/position";

type TrailProps = {
  droneCoords: GeoPosition;
  showTrail: boolean;
};

export const TrailDrone: FC<TrailProps> = ({ droneCoords, showTrail }) => {
  const [trail, setTrail] = useState<GeoPosition[]>([]);

  useEffect(() => {
    if (showTrail) {
      setTrail((prevTrail) => [...prevTrail, droneCoords]);
    }
  }, [droneCoords, showTrail]);

  return (
    <>
      {trail.map((pos, index) => (
        <CircleMarker
          key={index}
          center={[pos.latitude, pos.longitude]}
          radius={2}
          fillColor="red"
          color="red"
        />
      ))}
    </>
  );
};

type ToggleTrailButtonProps = {
  toggleShowTrail: () => void;
  showTrail: boolean;
};

export const ToggleTrailButton: React.FC<ToggleTrailButtonProps> = ({
  toggleShowTrail,
  showTrail,
}) => (
  <button
    style={{
      position: "absolute",
      bottom: "10rem",
      left: "1rem",
      zIndex: 1000,
    }}
    onClick={toggleShowTrail}
  >
    {showTrail ? "Hide Trail" : "Show Trail"}
  </button>
);
type ClearTrailButtonProps = {
  clearTrail: () => void;
};

export const ClearTrailButton: React.FC<ClearTrailButtonProps> = ({
  clearTrail,
}) => (
  <button
    style={{
      position: "absolute",
      bottom: "8rem",
      left: "1rem",
      zIndex: 1000,
    }}
    onClick={clearTrail}
  >
    Clear Trail
  </button>
);
