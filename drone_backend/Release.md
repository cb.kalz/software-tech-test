# Release

#### builds

```bash
# for reference https://go.dev/blog/pgo

# release build with no debug flags
go build -ldflags "-s -w" -o simple

# By default the compiler detects any profile default.go in main - and uses it to build

# To Confirm , build without pgo flags and check if -pgo was applied
go build -ldflags "-s -w" -o simple
go version -m simple


# static linking
CGO_ENABLED=0 go build -ldflags "-s -w" -o app


cd application/functional
go build -o drone_functional -pgo=default.pgo main.go
go version -m drone_functional

cd application/generic
go build -o drone_generic -pgo=default.pgo main.go
go version -m drone_generic


cd application/simple
go build -o drone_simple -pgo=default.pgo main.go
go version -m drone_simple
```

#### pprof

```go
import _ "net/http/pprof"

go func() {
    log.Println(http.ListenAndServe("localhost:6060", nil))
}()


```

<br/>

Optionally check profiles using pprof - I prefer to generate the profiles using release splits and saving them in case to check heap allocations and catch other issues asynchronously.

<br/>

```bash
# cpu profile
go tool pprof http://localhost:6060/debug/pprof/profile?seconds=30


# heap profile
go tool pprof http://localhost:6060/debug/pprof/heap

# contention profile
go tool pprof http://localhost:6060/debug/pprof/block

# goroutine profile
go tool pprof http://localhost:6060/debug/pprof/goroutine

# ui

go tool pprof -http=:8082 http://localhost:6060/debug/pprof/heap

web

go tool pprof -proto a.pprof b.pprof > merged.pprof

```
