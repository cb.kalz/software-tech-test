# Overview

**Contains the solution/refactor according to the given requirements**

```bash
application/microservices
```

<br/>

<hr>

#### core logic

```bash
core/        : Interfaces to make the codebase extensible and logic reusable
drone/       : Main Drone and Logic - we can consider this an "entity"
geo/         : Package defining the Abstraction for a Coordinate System and Math
measurement/ : Math Related to Lat/Lng
```

<br/>

<hr>

#### system logic

```bash
client/     : Impl of a Client usable in pub/sub contexts
publisher/  : Publisher impl - to enable us to create swappable/extensible Publishers
subscriber/ : Loosely Coupled Subscriber Implementation
apiserver/  : Server implementation for use with websockets and HTTP

```

<br/>

<hr>

#### build and run

Check **Release.md**

<br/>

<hr>

Additionally I have created 3 other sample implementations - as an illustration of the benefits of using Composition and Interfaces to drive design.

Refactoring and exploring new patterns becomes manageable without making Quality compromises for velocity

```bash
application/functional : coupling the Drone with the entire Service
application/generic    : sample of how we can have the components be swappable
application/simple     : purely procedural/imperative implementation - has benefits too

# the negatives of coupling the entire app as a single process would be cascanding failures among others - those are just examples to show the benefits of approaching the codebase
```

The microservices approach would simulate the Real World Scenario I believe - where the Drone has a mechanism and protocol to emit data to a centralized place. From there would have n ways of fanning out the Data and Distributing it.

## further considerations

Load Testing All Services

Load Testing the Redis Instance

Implementing Logic for Update Contention and Invalidating Stale Updates

Telemetry and Observability
