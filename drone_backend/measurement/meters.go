package measurement

import (
	"math"
)

type Meters float64

const (
	MetersPerKilometer  = 1000.0
	MetersPerMile       = 1609.34
	DegreesPerRadian    = 180 / math.Pi
	EarthRadiusInMeters = 6371000.0 // Average radius of the Earth in meters
)

func (m Meters) Float() float64 {
	return float64(m)
}

func (m Meters) FromMeters(meters float64) GeographicMeasurement {
	return Meters(meters)
}

func (m Meters) FromDegrees(degrees float64) GeographicMeasurement {
	return Meters(Meters(degrees * 111139.0))
}

func (m Meters) ToMeters() Meters {
	return m
}

// Note: For using with Latitude & Longitude - use ToDegreeLng() as the Longitudes vary due to the uneven nature of Earth's Curvature
func (m Meters) ToDegrees() Degrees {
	return Degrees(m / 111320.0)
}

func (m Meters) ToDegreeLat() Degrees {
	return Degrees(m / 111320.0)
}

// Accounts for the Earth's Curvature according to the Latitude value - and returns the Adjusted Longitude
func (m Meters) ToDegreesLng(lat float64) Degrees {
	return Degrees(float64(m) / (111139.0 * math.Cos(lat*math.Pi/180)))
}

func (m Meters) ToKilometers() float64 {
	return float64(m) / 1609.34
}

func (m Meters) ToMiles() float64 {
	return float64(m) / 1000
}

// Calculates if a Degree A , B are within acceptable bounds decimals included
// Usage:
//
//		computed := 0.006
//		a := Degree(0.0005001)
//	  b := Degree(0.0005222)
//		a.Equals(b,4)
func (m Meters) Equals(other Meters, decimalPlaces int) (bool, float64) {
	precision := math.Pow(10, -float64(decimalPlaces))
	difference := math.Abs(m.Float() - other.Float())
	isEqual := difference <= precision
	return isEqual, difference
}
