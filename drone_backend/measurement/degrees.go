package measurement

import "math"

type Degrees float64

func (d Degrees) Float() float64 {
	return float64(d)
}

func (d Degrees) FromMeters(meters float64) GeographicMeasurement {
	return Degrees(meters / 111139.0)
}

func (d Degrees) FromDegrees(float64) GeographicMeasurement {
	return Degrees(d)
}

func (d Degrees) ToMeters() Meters {
	return Meters(d * 111139.0)
}

func (d Degrees) ToKilometers() float64 {
	return float64(Meters(d * 111139.0).ToKilometers())
}

func (d Degrees) ToMiles() float64 {
	return float64(Meters(d * 111139.0).ToMiles())
}

func (d Degrees) ToDegrees() Degrees {
	return Degrees(d)
}

// Calculates if a Degree A , B are within acceptable bounds decimals included
// Usage:
//
//		computed := 0.006
//		a := Meters(1777.2555)
//	  b := Meters(1777.2221)
//		a.Equals(b,1)
func (d Degrees) Equals(other Degrees, decimalPlaces int) (bool, float64) {
	precision := math.Pow(10, -float64(decimalPlaces))
	difference := math.Abs(d.Float() - other.Float())
	isEqual := difference <= precision
	return isEqual, difference
}
