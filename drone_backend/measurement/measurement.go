package measurement

type GeographicMeasurement interface {
	FromMeters(float64) GeographicMeasurement
	FromDegrees(float64) GeographicMeasurement
	ToMeters() Meters
	ToDegrees() Degrees
	ToKilometers() float64
	ToMiles() float64
	Float() float64
}
