package core

import "droneshield/geo"

type ServiceType int

const (
	PublisherSvc ServiceType = iota
	SubscriberSvc
	LoggerSvc
)

type Service interface {
	GetServiceType() ServiceType
}

type ClientType int

const (
	HTTP ClientType = iota
	CLI
	InMemory
	Persistent
)

type EntityType int

const (
	RealTime EntityType = iota
	Asynchronous
)

type Entity interface {
	GetEntityType() EntityType
}

type WebService interface {
	Expose()
}

type Route interface {
	GetPath() string
	addPath() error
	addMethod() error
}

type CoordinatePublisher interface {
	EmitCoords() geo.GeoPoint
}
