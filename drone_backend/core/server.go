package core

import "context"

type APIService interface {
	NewAPIServer() APIServer
}

// Connection abstracts the communication channel to a client - so we can create
// fn's that use any type of connections , we would simply need to create Wrappers

type Connection interface {
	Send(message []byte) error
	Receive() ([]byte, error)
	Close() error
}

// In case we have web server like functionality we need to add further on
type Server interface {
	Start() error
	Stop() error
	RegisterRoute(path string, handlerFunc interface{})
	GetAddr() string
}

type PubSubAPIServer interface {
	APIServer

	AddConsumer(ctx context.Context,
		path string, subService SubscriberService,
		entity PubSubEntity, client PubSubClient)
}

type APIServer interface {
	Start() error
	Stop()
	GetAddr() string
	RegisterRoute(path string, handler interface{})
}
