package core

import (
	"context"

	"github.com/gorilla/websocket"
)

type DroneShieldService interface {
	GetEntity() Entity
	Run() error
	Stop() error
	ListServices() []Service
	ListClients() []Client
}

type DroneShieldPubSubSvc interface {
	DroneShieldService
	Publish()
	Subscribe()
	GetClient() Client
	RunPubSubService()
}

type PubSub interface {
	AddPublisher(publisher Publisher) PubSub
	AddSubscriber(subscriber Subscribable) PubSub
	Start(ctx context.Context) error
}

type PublisherBuilder interface {
	NewPublisher() Publisher
}

type PublisherService interface {
	NewPublisher() Publisher
}

/*
A Publisher that can Publish() and is coupled with a Client

In this context used as an abstraction over having a Redis Client with a Publish() method
*/
type Publisher interface {
	Service
	Publish(context.Context)
	PingClient(context.Context) (bool, error)
}

type SubscriberService interface {
	NewSubscriber() Subscribable
}

type SubscriberBuilder interface {
	NewSubscriber() SocketSubscribable
}

type SocketSubscribable interface {
	Service
	Subscribe(ctx context.Context, conn *websocket.Conn) error
}

type Subscribable interface {
	Service
	Subscribe(ctx context.Context, conn Connection, channel string) error
}

type Pollable interface {
	Poll(context.Context)
}

type Pingable interface {
	Ping() error
}

/*
Interface for Clients usable in Pub/Sub Services

This allows us to easily develop an Application with a Redis Client for example - and
later down the line we might need horizontal Scaling & Persistence : and require a Kafka client

From any existing entry point - we would then simply need to create a Kafka Client
that would implement this interface - and use it in place of the Redis Client.

Modeled based on *redis.Client
*/
type PubSubClient interface {
	Publish(ctx context.Context, channel string, message []byte) error
	Subscribe(ctx context.Context, channel string) PubSubStream
	Ping(ctx context.Context) error
}

type PubSubStream interface {
	Close() error
	Channel() <-chan PubSubMessage
}

type PubSubMessage interface {
	Payload() []byte
}

type PubSubEntity interface {
	Entity
	PublishSerializedEvent() ([]byte, error)
	HandleAPIRequest(message []byte) ([]byte, error)
	GetDefaultChannel() string
}

type Client interface {
	Connect(ctx context.Context) error
	GetClientType() []ClientType
}

type WSocketRedisSubscribable interface {
	Subscribe(ctx context.Context, wsConn *websocket.Conn)
	SubscribeToChannel(ctx context.Context, channel string, wsConn *websocket.Conn) error
	ListChannels() []string
	AddChannel(string) error
}
