# API

### http

```bash
# Directional Movement

curl -X POST http://localhost:8080/api \
     -H "Content-Type: application/json" \
     -d '{"action": "changePattern", "pattern": "Straight", "params": {"direction": "North"}}'

# Circular Movement
curl -X POST http://localhost:8080/api \
     -H "Content-Type: application/json" \
     -d '{"action": "changePattern", "pattern": "Circular", "params": {"center": {"latitude": -33.94755661744177, "longitude": 151.17470741271973}}}'

# ZigZag Movement
curl -X POST http://localhost:8080/api \
     -H "Content-Type: application/json" \
     -d '{"action": "changePattern", "pattern": "ZigZag", "params": {"target": {"latitude": -33.93755661744177, "longitude": 151.17470741271973}, "angle": 45.0, "increment": 0.001}}'

# Navigate to a Coordinate
curl -X POST http://localhost:8080/api \
     -H "Content-Type: application/json" \
     -d '{"action": "goTo", "params": {"target": {"latitude": -33.93755661744177, "longitude": 151.17470741271973}}}'

```

### websockets

```bash
# straight north/south/east/west

echo '{"action": "changePattern", "pattern": "Straight", "params": {"direction": "North"}}' | websocat ws://localhost:8081/


# circular movement

echo '{"action": "changePattern", "pattern": "Circular", "params": {"center": {"latitude": -33.94755661744177, "longitude": 151.17470741271973}}}' | websocat ws://localhost:8081/

# navigate to location

echo '{"action": "goTo", "params": {"target": {"latitude": 3.0, "longitude": 4.0}}}' | websocat ws://localhost:8081/

# zigzag

echo '{"action": "changePattern","pattern": "ZigZag","params": {"target": {"latitude": -33.93755661744177,"longitude": 151.17470741271973},"angle": 45.0,"increment": 0.001}}' | websocat ws://localhost:8081/
```

#### Note

Currently the Frontend implements HTTP as a fallback for WS which is a common pattern

However, for Subscribing to the Current Position HTTP would not be ideal as the latency to connect through the ISP over Fiber alone is typically 50-150ms and we can double/triple that assuming TLS and no HTTp/2.

A solution for this would be buffering the coordinates and sending them in buffers of 10+ or 3s+ increments.

For Req/Response kind of situations HTTP is ideal.

# ds

Each client maintains its own conn with a Redis - should load test how many clients we can support.

Redis will usually be constrained by the host system's memory as it is fully in-memory

```bash
# debugging lldb

go build -gcflags="all=-N -l" -o main

lldb main
breakpoint set --name main.main
run
n
n
s

```

#### dev deps

```bash
* redis

brew install redis
redis-server # run redis foreground
redis-cli shutdown # shutdown

* ws

websocat # check API.md for ws commands
websocat ws://localhost:8080/

```
