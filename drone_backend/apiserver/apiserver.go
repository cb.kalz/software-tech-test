package apiserver

import (
	"context"
	"log"
	"net/http"
	"strings"

	"droneshield/logger"

	"github.com/gorilla/websocket"
)

/*

API Server that we can use to Launch Web Servers and Expose Routes


*/

type ServerConfig struct {
	HTTPPort      string
	WebSocketPort string
}

type APIServer struct {
	HttpAddr      string
	WebsocketAddr string
	httpMux       *http.ServeMux
	websocketMux  *http.ServeMux
	upgrader      websocket.Upgrader
}

/*
Creates an API Server that can serve data and respond to requests using TCP over HTTP and Websockets

Usage:

	// API Server that we can use to Run different types of Servers
	apiServer := apiserver.NewAPIServer(apiserver.ServerConfig{
		HTTPPort:      ":8080",
		WebSocketPort: ":8082",
	})
*/
func NewAPIServer(cfg ServerConfig) *APIServer {
	httpPort, wsPort := validatePorts(cfg)

	return &APIServer{
		HttpAddr:      httpPort,
		WebsocketAddr: wsPort,
		httpMux:       http.NewServeMux(),
		websocketMux:  http.NewServeMux(),
		upgrader:      websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }},
	}
}

func (s *APIServer) AddHTTPRoute(pattern string, handler func(w http.ResponseWriter, r *http.Request)) {
	s.httpMux.HandleFunc(pattern, handler)
}

func (s *APIServer) AddWebSocketRoute(pattern string, handler func(conn *websocket.Conn, r *http.Request)) {
	s.websocketMux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		conn, err := s.upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("WebSocket upgrade failed: %v", err)
			return
		}
		defer conn.Close()
		handler(conn, r)
	})
}

func (s *APIServer) StartHTTPServer() {
	if err := http.ListenAndServe(s.HttpAddr, s.httpMux); err != nil {
		log.Fatalf("Failed to start HTTP server: %v", err)
	}
}

func (s *APIServer) StartWebSocketServer() {
	if err := http.ListenAndServe(s.WebsocketAddr, s.websocketMux); err != nil {
		log.Fatalf("Failed to start WebSocket server: %v", err)
	}
}

/* Blocking Call to run the API Server  */
func (s *APIServer) RunBlocking(ctx context.Context) {
	go s.StartHTTPServer()
	go s.StartWebSocketServer()

	logger.LogSectionBlueAlt("API Server Running")
	log.Printf("HTTP REST  Exposed Port %s%s%s", logger.BOLD, s.HttpAddr, logger.NC)
	log.Printf("WebSockets Exposed Port %s%s%s", logger.BOLD, s.WebsocketAddr, logger.NC)

	<-ctx.Done() // Wait for context cancellation to proceed with shutdown.

	// block := make(chan struct{})
	// <-block
}

func (s *APIServer) StartServers() {
	go func() {
		log.Printf("Starting HTTP server on %s", s.HttpAddr)
		if err := http.ListenAndServe(s.HttpAddr, s.httpMux); err != nil {
			log.Fatalf("Failed to start HTTP server: %v", err)
		}
	}()
	log.Printf("Preparing to start WebSocket server on %s", s.WebsocketAddr)
	go func() {
		log.Printf("Starting WebSocket server on %s", s.WebsocketAddr)
		if err := http.ListenAndServe(s.WebsocketAddr, s.websocketMux); err != nil {
			log.Fatalf("Failed to start WebSocket server: %v", err)
		}
	}()
}

func validatePorts(cfg ServerConfig) (string, string) {
	httpAddr := cfg.HTTPPort
	if !strings.HasPrefix(httpAddr, ":") {
		httpAddr = ":" + httpAddr
	}
	websocketAddr := cfg.WebSocketPort
	if !strings.HasPrefix(websocketAddr, ":") {
		websocketAddr = ":" + websocketAddr
	}

	return httpAddr, websocketAddr
}
