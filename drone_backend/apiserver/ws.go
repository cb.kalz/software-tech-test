package apiserver

import (
	"encoding/json"
	"log"

	"github.com/gorilla/websocket"
)

type WebSocketMessage struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

// WriteSuccess sends a success message to the WebSocket client
func WriteSuccess(conn *websocket.Conn, message string) {
	response := WebSocketMessage{
		Type:    "status",
		Message: message,
	}
	responseJSON, err := json.Marshal(response)
	if err != nil {
		log.Printf("Error marshalling success message: %v", err)
		return
	}
	if writeErr := conn.WriteMessage(websocket.TextMessage, responseJSON); writeErr != nil {
		log.Printf("Error sending success message to WebSocket client: %v", writeErr)
	}
}

// WriteError sends an error message to the WebSocket client
func WriteError(conn *websocket.Conn, errMsg string) {
	response := WebSocketMessage{
		Type:    "error",
		Message: errMsg,
	}
	responseJSON, err := json.Marshal(response)
	if err != nil {
		log.Printf("Error marshalling error message: %v", err)
		return
	}
	if writeErr := conn.WriteMessage(websocket.TextMessage, responseJSON); writeErr != nil {
		log.Printf("Error sending error message to WebSocket client: %v", writeErr)
	}
}
