package apiserver

import (
	"droneshield/core"

	"github.com/gorilla/websocket"
)

/*

Example of Implementing the core/Connection interface

For example if we have a new gRPC endpoint - and we need to integrate it in our existing solution

We would simply make sure the gRPC implementation implements the same Interface for its' methods


*/

type WebSocketConnection struct {
	conn *websocket.Conn
}

func (wsc *WebSocketConnection) Send(message []byte) error {
	return wsc.conn.WriteMessage(websocket.TextMessage, message)
}

func (wsc *WebSocketConnection) Receive() ([]byte, error) {
	_, message, err := wsc.conn.ReadMessage()
	return message, err
}

func (wsc *WebSocketConnection) Close() error {
	return wsc.conn.Close()
}

func NewWebSocketConnection(conn *websocket.Conn) core.Connection {
	return &WebSocketConnection{conn: conn}
}
