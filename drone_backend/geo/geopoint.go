package geo

import (
	"fmt"
	"math"

	"droneshield/measurement"
)

type GeoPoint interface {
	GetLat() float64
	GetLng() float64
	SetLat(float64)
	SetLng(float64)
	SetLatLng(float64, float64)
	UpdateLatLng(GeoPoint)

	IncrementLatLng(float64, float64)
	DecrementLatLng(float64, float64)
	IncrementLat(float64)
	IncrementLng(float64)
	DecrementLat(float64)
	DecrementLng(float64)

	DegreesBetween(p2 GeoPoint) float64
	CalculateBearing(l2 GeoPoint) float64
	DistanceToInMeters(p2 GeoPoint) float64
	AngleOnCircleEdgeWithCenter(l2 GeoPoint) float64
	GetCircleCircumference(l2 GeoPoint) measurement.Meters
}

type LatLng struct {
	Lat float64 `json:"latitude"`
	Lng float64 `json:"longitude"`
}

func NewLatLng(lat, lng float64) GeoPoint {
	return &LatLng{Lat: lat, Lng: lng}
}

func (p LatLng) GetLat() float64 {
	return p.Lat
}

func (p LatLng) GetLng() float64 {
	return p.Lng
}

func (p *LatLng) SetLatLng(newLat, newLng float64) {
	p.SetLat(newLat)
	p.SetLng(newLng)
}

func (p *LatLng) UpdateLatLng(newPoint GeoPoint) {
	p.SetLatLng(newPoint.GetLat(), newPoint.GetLng())
}

func (p *LatLng) IncrementLatLng(newLat, newLng float64) {
	p.Lat += newLat
	p.Lng += newLng
}

func (p *LatLng) DecrementLatLng(newLat, newLng float64) {
	p.Lat -= newLat
	p.Lng -= newLng
}

func (p *LatLng) SetLat(lat float64) {
	p.Lat = lat
}

func (p *LatLng) SetLng(lng float64) {
	p.Lng = lng
}

func (p *LatLng) IncrementLat(lat float64) {
	p.Lat += lat
}

func (p *LatLng) IncrementLng(lng float64) {
	p.Lng += lng
}

func (p *LatLng) DecrementLat(lat float64) {
	p.Lat -= lat
}

func (p *LatLng) DecrementLng(lng float64) {
	p.Lng -= lng
}

func (l1 LatLng) DistanceTo(l2 GeoPoint) measurement.Meters {
	return measurement.Meters(CalculateDistanceMeters(&l1, l2))
}

func (l1 LatLng) DistanceToPointMetersWithinThreshold(l2 GeoPoint, threshold float64) bool {
	dist := measurement.Meters(CalculateDistanceMeters(&l1, l2))
	return dist.Float() < threshold
}

func (l1 LatLng) DistanceToPointDegreesWithinThreshold(l2 GeoPoint, threshold float64) (bool, float64) {
	dist := l1.DegreesBetween(l2)
	difference := math.Abs(dist - threshold)
	isWithinThreshold := difference <= math.Pow(10, -3) // Assuming 3 decimal places as per your Equals method
	return isWithinThreshold, difference
}

func (l1 LatLng) GetCircleCircumference(l2 GeoPoint) measurement.Meters {
	return measurement.Meters(CalculateDistanceMeters(&l1, l2) * math.Pi * 2)
}

/*

radius_meters := geo.CalculateDistanceMeters(startPoint, centerPoint)

*/

func (l1 LatLng) DegreesBetween(l2 GeoPoint) float64 {
	return CalculateDistanceDegrees(&l1, l2)
}

func (l1 LatLng) DistanceToInMeters(l2 GeoPoint) float64 {
	return CalculateDistanceMeters(&l1, l2)
}

func (l1 LatLng) AngleOnCircleEdgeWithCenter(l2 GeoPoint) float64 {
	return InitialAnglePointsCircle(&l1, l2)
}

func (l1 LatLng) CalculateBearing(l2 GeoPoint) float64 {
	return CalculateBearing(l1.GetLat(), l1.GetLng(), l2.GetLat(), l2.GetLng())
}

func (l LatLng) IsLocationValid() bool {
	if l.GetLat() > 90.0 || l.GetLat() < -90.0 ||
		l.GetLng() > 180.0 || l.GetLng() < -180.0 {
		return false
	}
	return true
}

func (l LatLng) ValidateLatLng() (bool, error) {
	if l.GetLat() > 90.0 || l.GetLat() < -90.0 {
		return false, fmt.Errorf("Longitude is larger than valid range -90 to 90. Latitude Recv:%f", l.GetLat())
	}
	if l.GetLng() > 180.0 || l.GetLng() < 180.0 {
		return false, fmt.Errorf("Longitude is larger than valid range -180 to 180 Longitude Recv:%f", l.GetLng())
	}
	return true, nil
}
