package geo

import (
	"math"
	"time"

	"droneshield/measurement"
)

type Measurement interface {
	Metric()
	Degree()
}

// Euclidean distance between two points.
func DistanceEuclid(p1, p2 GeoPoint) float64 {
	return math.Sqrt(math.Pow(p1.GetLat()-p2.GetLng(), 2) + math.Pow(p1.GetLat()-p2.GetLng(), 2))
}

func CalculateSpeedMetersPerSecond(latestPos, currPos GeoPoint, currTimestamp, latestTimestamp time.Time) float64 {
	return CalculateDistanceMeters(currPos, latestPos) / (currTimestamp.Sub(latestTimestamp).Seconds())
}

// m/s * 3.6 -> km/h
func CalculateSpeedKmPerHour(latestPos, currPos GeoPoint, currTimestamp, latestTimestamp time.Time) float64 {
	speedMetersPerSecond := CalculateDistanceMeters(currPos, latestPos) / (currTimestamp.Sub(latestTimestamp).Seconds())
	return speedMetersPerSecond * 3.6
}

// m/s * 2.23694 -> mph
func CalculateSpeedMilesPerHour(latestPos, currPos GeoPoint, currTimestamp, latestTimestamp time.Time) float64 {
	speedMetersPerSecond := CalculateDistanceMeters(currPos, latestPos) / (currTimestamp.Sub(latestTimestamp).Seconds())
	return speedMetersPerSecond * 2.23694
}

func CalculateDistanceMiles(l1, l2 GeoPoint) float64 {
	return CalculateDistanceMeters(l1, l2) / 1609.34
}

func CalculateDistanceKmph(l1, l2 GeoPoint) float64 {
	return CalculateDistanceMeters(l1, l2) / 111139.0
}

func CalculateDistanceDegrees(l1, l2 GeoPoint) float64 {
	return CalculateDistanceMeters(l1, l2) / 111139.0
}

func CalculateDistanceMeters(l1, l2 GeoPoint) float64 {
	// Convert the latitudes and longitudes from degrees to radians
	lat1 := LatOrLngToRadians(l1.GetLat())
	long1 := LatOrLngToRadians(l1.GetLng())
	lat2 := LatOrLngToRadians(l2.GetLat())
	long2 := LatOrLngToRadians(l2.GetLng())

	// Haversine Formula
	dlong := long2 - long1
	dlat := lat2 - lat1

	ans := math.Pow(math.Sin(dlat/2), 2) +
		math.Cos(lat1)*math.Cos(lat2)*math.Pow(math.Sin(dlong/2), 2)

	ans = 2 * math.Asin(math.Sqrt(ans))

	// Radius of Earth in Kilometers, R = 6371
	R := 6371

	// Calculate the result
	ans = ans * float64(R)

	return ans * 1000.0
}

func CalculateDistanceMetersMeasurement[T measurement.GeographicMeasurement](l1, l2 GeoPoint) float64 {
	// Convert the latitudes and longitudes from degrees to radians
	lat1 := LatOrLngToRadians(l1.GetLat())
	long1 := LatOrLngToRadians(l1.GetLng())
	lat2 := LatOrLngToRadians(l2.GetLat())
	long2 := LatOrLngToRadians(l2.GetLng())

	// Haversine Formula
	dlong := long2 - long1
	dlat := lat2 - lat1

	ans := math.Pow(math.Sin(dlat/2), 2) +
		math.Cos(lat1)*math.Cos(lat2)*math.Pow(math.Sin(dlong/2), 2)

	ans = 2 * math.Asin(math.Sqrt(ans))

	// Radius of Earth in Kilometers, R = 6371
	R := 6371

	// Calculate the result
	ans = ans * float64(R)

	calc := ans * 1000.0

	var t T
	return t.FromMeters(calc).Float()
}
