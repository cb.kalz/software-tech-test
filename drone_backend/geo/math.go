package geo

import (
	"math"
)

const (
	degree_in_meters = 111319.0
)

/*
meters to degrees of latitude

1 degree of latitude ~ 111.32 kilometers

1 degree of longitude at the equator ~ 111.32 kilometers

But it decreases as you move towards the poles

So, meters / (111320 * cos(latitude in radians)) is the change in longitude

speed

m/s * 2.23694 ->  mph
m/s * 3.6 		-> kmph

*/

func MetersToDegreeForLat(meters float64) float64 {
	return meters / degree_in_meters
}

func MetersToDegreeForLong(meters float64, latitude float64) float64 {
	return meters / (degree_in_meters * math.Cos(latitude*math.Pi/180))
}

func ConvertMpsMph(speed float64) float64 {
	return speed * 2.23694
}

func ConvertMpsKmph(speed float64) float64 {
	return speed * 2.23694
}

func LatOrLngToRadians(lat float64) float64 {
	return lat / (180 / math.Pi)
}

func KilometersToMiles(kilometers float64) float64 {
	return kilometers / 1.60934
}

func FloatsEqual(a, b, precision float64) bool {
	return math.Abs(a-b) <= precision
}

// Calculates if a Float A , B are within acceptable bounds decimals included
// Usage:
//
//	computed := 0.006
//	geo.FloatEqualDecimalPrecision(computed, 0.0, 3) -> true  , 0.000671
//	geo.FloatEqualDecimalPrecision(computed, 0.0, 4) -> false , 0.000671
func FloatEqualDecimalPrecision(a, b float64, decimalPlaces int) (bool, float64) {
	precision := math.Pow(10, -float64(decimalPlaces))
	difference := math.Abs(a - b)
	isEqual := difference <= precision
	return isEqual, difference
}

func IsDistanceWithinPrecision(a, b float64, decimalPlaces int) (bool, float64) {
	precision := math.Pow(10, -float64(decimalPlaces))
	difference := math.Abs(a - b)
	isEqual := difference <= precision
	return isEqual, difference
}

func CalculateBearing(xA, yA, xB, yB float64) float64 {
	lat1 := xA * math.Pi / 180
	lat2 := xB * math.Pi / 180
	dLon := (yB - yA) * math.Pi / 180

	x := math.Sin(dLon) * math.Cos(lat2)
	y := math.Cos(lat1)*math.Sin(lat2) - math.Sin(lat1)*math.Cos(lat2)*math.Cos(dLon)
	bearing := math.Atan2(x, y) * 180 / math.Pi

	bearing = math.Mod(bearing+360, 360)
	return bearing
}

func MetersToRadiusRadiansCurvature(meters float64) float64 {
	earthRadiusMeters := 6371000.0 // Earth's radius in meters
	return (meters / earthRadiusMeters) * (180 / math.Pi)
}

func MetersToRadiusDegrees(meters float64) float64 {
	return meters / degree_in_meters
}

func DegreesToMeters(degrees float64) float64 {
	return degrees * degree_in_meters
}
