package geo

import "math"

func GetRadiusInDegrees(p1, p2 GeoPoint) float64 {
	dist_meters := CalculateDistanceMeters(p1, p2)
	return MetersToRadiusRadiansCurvature(dist_meters)
}

func AngularIncrement(p1, p2 GeoPoint, degreeIncrement float64) float64 {
	return DegreesToMeters(degreeIncrement) / CalculateDistanceMeters(p1, p2)
}

func GeoMidPoint(l1, l2 GeoPoint) GeoPoint {
	return &LatLng{
		((l1.GetLat() + l2.GetLat()) / 2), ((l1.GetLng() + l2.GetLng()) / 2),
	}
}

func InitialAnglePointsCircle(edgePoint, centerPoint GeoPoint) float64 {
	deltaLat := edgePoint.GetLat() - centerPoint.GetLat()
	deltaLng := edgePoint.GetLng() - centerPoint.GetLng()

	initialAngle := math.Atan2(deltaLng, deltaLat)

	if initialAngle < 0 {
		initialAngle += 2 * math.Pi
	}
	return initialAngle
}

// Given GeoPoint p1 and p2 where p2 is center and we want p1 to rotate around p2

// 1. function to get Radius in Meters between 2 Geo Points
func GetCircleRadius(p1, p2 GeoPoint) float64 {
	return CalculateDistanceMeters(p1, p2)
}

// 2. Generate the Points around the Circumference of the Given Circle
// func GetZoneCoordinates(center GeoPoint, radiusInMiles float64, numberOfPoints int) []GeoPoint {
// 	var points []GeoPoint

// 	radiusKm := radiusInMiles / 0.621371
// 	radiusLon := (1 / (111.319 * math.Cos(center.GetLat()*math.Pi/180))) * radiusKm
// 	radiusLat := (1 / 110.574) * radiusKm

// 	// Calculate the amount to increment the angle for the number of points
// 	dTheta := 2 * math.Pi / float64(numberOfPoints)
// 	var theta float64

// 	for i := 0; i < numberOfPoints; i++ {
// 		newLat := center.GetLat() + radiusLat*math.Sin(theta)
// 		newLon := center.GetLng() + radiusLon*math.Cos(theta)
// 		points = append(points, &LatLng{Lat: newLat, Lng: newLon})
// 		theta += dTheta
// 	}

// 	return points
// }
