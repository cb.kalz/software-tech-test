package drone

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"droneshield/geo"
	"droneshield/logger"
	"droneshield/subscriber"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

type CommandParams interface {
	Apply(drone *Drone)
}

/*
Commands to Modify Drone behavior, Serialized from JSON strings

Available Commands:

	Circular -> Issue the Drone to move in a Circular Pattern around a specified Point

	ZigZag   -> Issue the Drone to move in a Zig-Zag pattern specified by a Point and Angle

	GoTo     -> Directly move towards a Point

	Random	 -> Issue the Drone to move with Randomized movements
*/
type Command struct {
	Action  string        `json:"action"`
	Pattern string        `json:"pattern"`
	Params  CommandParams `json:"params,omitempty"`
}

/* Interface Implementation to use as a Publisher Extensibly */
func (drone *Drone) PublishSerializedEvent() ([]byte, error) {
	data := drone.EmitCoords()
	return json.Marshal(data)
}

/* Handler for Network Connections */
type APIResponse struct {
	SuccessMessage string      `json:"successMessage"`
	EchoedRequest  interface{} `json:"echoedRequest,omitempty"`
	CurrentPattern string      `json:"currentPattern"`
}

func (drone *Drone) HandleAPIRequest(message []byte) ([]byte, error) {
	log.Printf("Received Command:%s%s%s", logger.PEACH, message, logger.NC)
	var cmd Command
	if err := json.Unmarshal(message, &cmd); err != nil {
		return nil, err
	}

	successMessage := "Command missing parameters"
	if cmd.Params != nil {
		cmd.Params.Apply(drone)
		successMessage = "Successfully updated drone movement pattern"
		log.Printf("%s%s%s to %s%s%s%s", logger.BOLD, successMessage, logger.NC, logger.BOLD, logger.PEACH, cmd.Pattern, logger.NC)
	}

	drone.RLock()
	currentPattern := drone.movementStrategyToStr()
	drone.RUnlock()

	response := APIResponse{
		SuccessMessage: successMessage,
		EchoedRequest:  cmd,
		CurrentPattern: currentPattern,
	}

	return json.Marshal(response)
}

type CircularParams struct {
	Center          *geo.LatLng `json:"center"`
	IncrementFactor *float64    `json:"increment,omitempty"`
}

func (cp *CircularParams) Apply(drone *Drone) {
	var incrementFactor float64

	if cp.IncrementFactor != nil {
		incrementFactor = *cp.IncrementFactor
	} else {
		incrementFactor = drone.incrementFactor
	}

	circular_config := NewCircularConfig(drone.currPos, cp.Center, incrementFactor)

	drone.ChangePattern(&circular_config)
}

type StraightParams struct {
	Direction string `json:"direction"`
}

func (sp *StraightParams) Apply(drone *Drone) {
	var dir Direction
	switch sp.Direction {
	case "North":
		dir = North
	case "South":
		dir = South
	case "East":
		dir = East
	case "West":
		dir = West
	}
	drone.ChangePattern(StraightMovement{Direction: dir})
}

type GoToParams struct {
	Target geo.LatLng `json:"target"`
}

func (gp *GoToParams) Apply(drone *Drone) {
	drone.SetGoToCoordinate(&gp.Target)
}

type ZigZagParams struct {
	Target          *geo.LatLng `json:"target"`
	Angle           float64     `json:"angle"`
	IncrementFactor *float64    `json:"increment,omitempty"`
}

func (zp *ZigZagParams) Apply(drone *Drone) {
	var inc float64
	if zp.IncrementFactor == nil {
		inc = 0.0
	} else {
		inc = *zp.IncrementFactor
	}
	zigZagConfig := ZigZagConfig{
		TargetPoint: zp.Target,
		Angle:       zp.Angle,
		Increment:   inc,
	}

	drone.ChangePattern(&zigZagConfig)
}

/*
Implementation of an API to interact with the Drone

Thread Safe implementation - calls can be made concurrently against a Pointer
of an instantiated drone

Concurrent Commands will be blocking in nature
i.e if 2 connected clients issue a Command to change Patterns - they will be applied on a FCFS basis

Usage:

	var cmd drone.Command

	if err := json.Unmarshal(message, &cmd); err != nil {
		logger.LogError(fmt.Sprintf("Error decoding command: %v", err))
		continue
	}

	logger.LogEvent("Received Command:", cmd, "Params:", cmd.Params)

	drone.UpdateDroneFromCmd(cmd, droneInstance)
*/
func UpdateDroneFromCmd(cmd Command, droneInstance *Drone) {
	if cmd.Params != nil {

		cmd.Params.Apply(droneInstance)
		log.Printf("%sSuccessfully%s Applied Drone Movement Pattern: %s%s%s%s", logger.BOLD, logger.NC, logger.BOLD, logger.PEACH, cmd.Pattern, logger.NC)
	} else {
		log.Printf("Command missing parameters")
	}
}

func (c *Command) UnmarshalJSON(data []byte) error {
	type Alias Command
	aux := &struct {
		*Alias
		Params json.RawMessage `json:"params,omitempty"`
	}{
		Alias: (*Alias)(c),
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	switch c.Action {
	case "changePattern":
		switch c.Pattern {
		case "Circular":
			var circularParams CircularParams
			if err := json.Unmarshal(aux.Params, &circularParams); err != nil {
				return err
			}
			c.Params = &circularParams
		case "Straight":
			var straightParams StraightParams
			if err := json.Unmarshal(aux.Params, &straightParams); err != nil {
				return err
			}
			c.Params = &straightParams
		case "ZigZag":
			var zigZagParams ZigZagParams

			if err := json.Unmarshal(aux.Params, &zigZagParams); err != nil {
				return err
			}
			c.Params = &zigZagParams
		}
	case "goTo":
		var goToParams GoToParams
		if err := json.Unmarshal(aux.Params, &goToParams); err != nil {
			return err
		}
		c.Params = &goToParams
	default:
		return fmt.Errorf("unsupported action or pattern: %s", c.Action)
	}

	return nil
}

func DroneTrackerPubSubHandler(ctx context.Context,
	conn *websocket.Conn,
	client *redis.Client,
	channel string,
	droneInstance *Drone,
) {
	/*   1. Each connection/consumer has their Subscription to the Redis Channel   */

	go func() {
		subscriber := subscriber.NewRedisChannelSubscriber(client)
		subscriber.Subscribe(ctx, conn, channel)
	}()

	/*    2. Process any incoming messages on the socket from the connected Client  */

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Printf("WebSocket read error: %v", err)
			break
		}

		var cmd Command

		if err := json.Unmarshal(message, &cmd); err != nil {
			logger.LogError(fmt.Sprintf("Error decoding command: %v", err))
			continue
		}

		logger.LogEvent("Received Command:", cmd, "Params:", cmd.Params)

		UpdateDroneFromCmd(cmd, droneInstance)

	}
}
