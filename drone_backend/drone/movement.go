package drone

import (
	"math"
	"math/rand"

	"droneshield/geo"
)

type Pattern int

const (
	Random Pattern = iota
	Straight
	Circular
	Triangular
	ZigZag
)

type MovementStrategy interface {
	Move(drone *Drone) geo.GeoPoint
}
type GoToCoordinateMovement struct {
	Target geo.GeoPoint
}

func (g *GoToCoordinateMovement) Move(drone *Drone) geo.GeoPoint {
	bearing := geo.CalculateBearing(drone.currPos.GetLat(), drone.currPos.GetLng(), g.Target.GetLat(), g.Target.GetLng())

	bearingRad := bearing * math.Pi / 180

	drone.currPos.SetLat(drone.currPos.GetLat() + drone.incrementFactor*math.Cos(bearingRad))

	drone.currPos.SetLng(drone.currPos.GetLng() + drone.incrementFactor*math.Sin(bearingRad))

	return drone.currPos
}

func (drone *Drone) SetGoToCoordinate(target geo.GeoPoint) {
	drone.Lock()
	defer drone.Unlock()

	drone.movementStrategy = &GoToCoordinateMovement{Target: target}
}

type RandomMovement struct{}

func (s RandomMovement) Move(drone *Drone) geo.GeoPoint {
	drone.currPos = &geo.LatLng{
		Lat: drone.currPos.GetLat() + rand.Float64()*drone.incrementFactor,
		Lng: drone.currPos.GetLng() + rand.Float64()*drone.incrementFactor,
	}
	return drone.currPos
}

type StraightMovement struct {
	Direction Direction
}

func (s StraightMovement) Move(drone *Drone) geo.GeoPoint {
	switch s.Direction {
	case North:
		drone.currPos.SetLat(drone.currPos.GetLat() + drone.incrementFactor)
	case South:
		drone.currPos.SetLat(drone.currPos.GetLat() - drone.incrementFactor)
	case East:
		drone.currPos.SetLng(drone.currPos.GetLng() + drone.incrementFactor)
	case West:
		drone.currPos.SetLng(drone.currPos.GetLng() - drone.incrementFactor)

	}
	return drone.currPos
}

type ZigZagMovement struct {
	StraightDistance float64
	DiagonalDistance float64
	DiagonalAngle    float64
	StartingLeg      ZigZagState
	currentLeg       ZigZagState
}

func (z *ZigZagMovement) Move(drone *Drone) geo.GeoPoint {
	if z.currentLeg == StraightLeg {

		switch drone.direction {
		case North:
			drone.currPos.IncrementLat(z.StraightDistance)
		case South:
			drone.currPos.DecrementLat(z.StraightDistance)
		case East:
			drone.currPos.IncrementLng(z.StraightDistance)
		case West:
			drone.currPos.DecrementLng(z.StraightDistance)

		}
		z.currentLeg = DiagonalLeg

	} else { /* Diagonal movement logic */

		radAngle := (z.DiagonalAngle) * (math.Pi / 180)
		drone.currPos.IncrementLat(math.Cos(radAngle) * z.DiagonalDistance)
		drone.currPos.IncrementLng(math.Sin(radAngle) * z.DiagonalDistance)
		z.currentLeg = StraightLeg
	}

	return drone.currPos
}

type Option func(MovementStrategy)
