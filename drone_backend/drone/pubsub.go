package drone

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"droneshield/apiserver"
	"droneshield/client"
	"droneshield/core"
	"droneshield/logger"
	"droneshield/publisher"
	"droneshield/subscriber"
	"droneshield/utilities"

	"github.com/gorilla/websocket"
)

type DronePubSubConfig struct {
	client     core.PubSubClient
	publisher  core.Publisher
	subscriber core.SubscriberBuilder
	apiServer  apiserver.APIServer
}

func (d *Drone) WithPubSub(redisAddr string) *Drone {
	if d.Config != nil && d.Config.client == nil {
		log.Printf("Creating PubSub Client")

		client, err := client.NewRedisPubSubClient(context.Background(), redisAddr)
		if err != nil {
			logger.LogError(fmt.Sprintf("Failed to connect to Redis Client at %s. ERROR: %s", redisAddr, err))
			os.Exit(1)
		}
		d.Config.client = client
		logger.LogStepSuccess("Successfully Created PubSub Client")
	} else {
		logger.LogWarning("Client is already set. Ignoring the new setting.")
	}

	pubBuilder := publisher.NewPublisherBuilder(d.Config.client, d)
	publisher := pubBuilder.NewPublisher()
	d.Config.publisher = publisher

	if d.GetDefaultChannel() == "" {
		d.SetPubSubChannel("drone_coordinates")
	}
	subBuilder := subscriber.NewSubscriberBuilder(d, d.Config.client)

	d.Config.subscriber = subBuilder

	return d
}

func (d *Drone) WithAPIServer(cfg apiserver.ServerConfig) *Drone {
	d.Config.apiServer = *apiserver.NewAPIServer(cfg)
	return d
}

func (d *Drone) WithChannel(channelName string) *Drone {
	d.SetPubSubChannel(channelName)
	return d
}

func (d *Drone) LaunchPubSub(ctx context.Context) (*Drone, error) {
	if utilities.AreAllNonNil(d.Config) == false {
		return d, fmt.Errorf("Client Configuration not Complete")
	}

	go d.Config.publisher.Publish(context.Background())

	if d.Config.apiServer.HttpAddr != "" && d.Config.apiServer.WebsocketAddr != "" {

		d.Config.apiServer.AddHTTPRoute("/api", func(w http.ResponseWriter, r *http.Request) {
			if r.Method != http.MethodPost {
				http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
				return
			}

			var cmd Command
			if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
				http.Error(w, fmt.Sprintf("Error decoding request: %v", err), http.StatusBadRequest)
				return
			}

			log.Printf("Recv %sHTTP POST%s:", logger.BOLD, logger.NC)

			reqData, err := json.Marshal(cmd)
			if err != nil {
				logger.LogWarning("Failed to Serialize HTTP Request")
				return
			}

			response, err := d.HandleAPIRequest(reqData)
			if err != nil {
				http.Error(w, fmt.Sprintf("Error processing command: %v", err), http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(response)
		})

		d.Config.apiServer.AddWebSocketRoute("/", func(conn *websocket.Conn, r *http.Request) {
			go func() {
				subscriber := d.Config.subscriber.NewSubscriber()

				subscriber.Subscribe(context.Background(), conn)
			}()

			for {
				_, message, err := conn.ReadMessage()
				if err != nil {
					log.Printf("WebSocket read error: %v", err)
					break
				}

				response, err := d.HandleAPIRequest(message)
				if err != nil {
					log.Printf("Error processing request: %v", err)
					continue
				}

				log.Printf("Response %s", response)

			}
		})

	} else {
		logger.LogWarning("API Server configuration is incomplete.")
	}

	// blocks, preventing the program from exiting
	d.Config.apiServer.RunBlocking(ctx)

	logger.LogImportant("API Server Shut Down")
	return d, nil
}
