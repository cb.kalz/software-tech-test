package drone

import (
	"math"

	"droneshield/geo"
)

type ZigZagPhase int

const (
	MovingStraight ZigZagPhase = iota
	MovingAngled
	Reversing
)

type ZigZagConfig struct {
	TargetPoint geo.GeoPoint
	Angle       float64
	Increment   float64
	Phase       ZigZagPhase
	initialized bool

	bearingRad         float64
	LegDistanceDegrees float64
	currLegSteps       int
	currStep           int
}

func (z *ZigZagConfig) initConfig(drone *Drone) {
	if z.Increment == 0 {
		z.Increment = drone.incrementFactor
	}
	z.LegDistanceDegrees = drone.currPos.DegreesBetween(z.TargetPoint)
	z.Phase = MovingStraight
	z.currLegSteps = int(math.Round(z.LegDistanceDegrees / z.Increment))
	z.bearingRad = drone.currPos.CalculateBearing(z.TargetPoint) * math.Pi / 180
	z.initialized = true

}

func (z *ZigZagConfig) Move(drone *Drone) geo.GeoPoint {
	if z.initialized == false {
		z.initConfig(drone)
	}

	// Move drone by increment at current bearing
	if z.currStep < z.currLegSteps {
		drone.currPos.IncrementLatLng(z.Increment*math.Cos(z.bearingRad), z.Increment*math.Sin(z.bearingRad))
		z.currStep++
	}

	if z.currStep >= z.currLegSteps {
		z.adjustBearingForZigzag()
		z.currStep = 0

		switch z.Phase {
		case MovingAngled:
			z.Phase = MovingStraight
			z.LegDistanceDegrees /= math.Sqrt(2)
		case MovingStraight:
			z.Phase = MovingAngled
			z.LegDistanceDegrees *= math.Sqrt(2)
		}

		z.currLegSteps = int(math.Round(z.LegDistanceDegrees / z.Increment))

	}

	return drone.currPos
}

// Use the Initia bearing and update it by provided angle to move in a Zig-Zag pattern
func (z *ZigZagConfig) adjustBearingForZigzag() {
	zigzagAngleRad := z.Angle * (math.Pi / 180)

	if z.Phase == MovingStraight {
		z.bearingRad = math.Mod(z.bearingRad+zigzagAngleRad, 2*math.Pi)
	} else {
		z.bearingRad = math.Mod(z.bearingRad-zigzagAngleRad+2*math.Pi, 2*math.Pi)
	}
}
