package drone

import (
	"math"

	"droneshield/geo"
)

type circularCorr struct {
	Center           geo.GeoPoint
	StartPoint       geo.GeoPoint
	radiusMeters     float64
	angularIncrement float64
	currAngle        float64
}

/*
Given a Start Position (x1,x2), Increment (ex:0.001) and Circle w/ center (x2,y2)

	-> Calculate the next Coordinate such that (x0,y0...xN,yN) is generated
		 to travel around the Circumference of the circle with center (x2,y2) w/ R dist(startCircle,endCircle)
*/
func NewCircularConfig(startPoint, centerPoint geo.GeoPoint, incrementDegrees float64) circularCorr {
	radius_meters := geo.CalculateDistanceMeters(startPoint, centerPoint)
	angularIncrement := geo.DegreesToMeters(incrementDegrees) / radius_meters
	initial_angle := geo.InitialAnglePointsCircle(startPoint, centerPoint)

	return circularCorr{
		Center:           centerPoint,
		StartPoint:       startPoint,
		radiusMeters:     radius_meters,
		angularIncrement: angularIncrement,
		currAngle:        initial_angle,
	}
}

// Moves the Object according to the Calculated Increments around the Circumference & Updates Current Angle
func (c *circularCorr) Move(drone *Drone) geo.GeoPoint {
	newAngle := c.currAngle + c.angularIncrement

	c.currAngle += c.angularIncrement

	if newAngle >= 2*math.Pi {
		newAngle -= 2 * math.Pi
	}

	// Calculate the new position
	newLat := c.Center.GetLat() + math.Cos(newAngle)*(c.radiusMeters/111139.0)
	newLng := c.Center.GetLng() + math.Sin(newAngle)*(c.radiusMeters/(111139.0*math.Cos(c.Center.GetLat()*math.Pi/180)))

	drone.SetLocation(&geo.LatLng{Lat: newLat, Lng: newLng})

	return drone.currPos
}
