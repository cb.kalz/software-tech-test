package drone

import (
	"fmt"
	"sync"
	"time"

	"droneshield/core"
	"droneshield/geo"
	"droneshield/logger"
)

type Direction int

const (
	North Direction = iota
	South
	East
	West
)

type ZigZagState int

const (
	StraightLeg ZigZagState = iota
	DiagonalLeg
)

type Movable interface {
	EmitCoords() geo.GeoPoint
	SetPattern(pattern Pattern, direction Direction)
	ReverseDirection()
}
type Incrementable interface {
	MovementStrategy
	SetIncrement(increment float64)
}

type Drone struct {
	Config          *DronePubSubConfig
	StartPosition   geo.GeoPoint
	currPos         geo.GeoPoint
	incrementFactor float64

	startTime       time.Time
	currSpeed       float64
	stationary      bool
	averageSpeed    float64
	totalDistance   float64
	latestTimestamp time.Time

	pattern          Pattern
	direction        Direction
	movementStrategy MovementStrategy

	pubsubChannel string
	debug_flag    bool
	sync.RWMutex
}

func (drone *Drone) EmitCoords() geo.GeoPoint {
	drone.Lock()

	// if the drone is set to be stationary - do not issue a move or compute speed & dist travelled
	if drone.stationary || drone.movementStrategy == nil {
		drone.Unlock()
		return drone.currPos
	}

	// move the drone , get dist travelled, and set curr speed & avg speed (**real world scenario timestamp will be stored on the drone internally)
	curr_loc := drone.currPos
	new_loc := drone.movementStrategy.Move(drone)

	distance := geo.CalculateDistanceMeters(curr_loc, new_loc)

	currTimestamp := time.Now()

	if timeElapsed := currTimestamp.Sub(drone.latestTimestamp).Seconds(); timeElapsed > 0 {

		drone.currSpeed = geo.ConvertMpsMph(distance / timeElapsed)
		drone.totalDistance += distance
		drone.averageSpeed = drone.totalDistance / timeElapsed
	}

	drone.latestTimestamp = currTimestamp
	drone.Unlock()

	return new_loc
}

func NewDrone(startPosition geo.GeoPoint) *Drone {
	return &Drone{
		StartPosition:    startPosition,
		startTime:        time.Now(),
		currPos:          startPosition,
		pattern:          Random,
		incrementFactor:  0.001,
		movementStrategy: RandomMovement{},
	}
}

func (drone *Drone) GetCurrentPosition() geo.GeoPoint {
	return drone.currPos
}

func (drone *Drone) GetDroneMetadata() geo.GeoPoint {
	drone.RLock()
	defer drone.RUnlock()
	return drone.currPos
}

func (drone *Drone) ChangePattern(strategy MovementStrategy) {
	drone.Lock()
	defer drone.Unlock()

	drone.movementStrategy = strategy
}

func (drone *Drone) ReverseDirection() {
	switch drone.pattern {

	case Circular:
		drone.incrementFactor = -drone.incrementFactor

	case Straight:
		switch drone.direction {
		case North:
			drone.direction = South
		case South:
			drone.direction = North
		case East:
			drone.direction = West
		case West:
			drone.direction = East
		}
	}
}

func (drone *Drone) SetLocation(new_loc geo.GeoPoint) {
	drone.currPos.UpdateLatLng(new_loc)
}

func (drone *Drone) SetDirection(dir Direction) {
	drone.direction = dir
}

func (drone *Drone) SetIncrement(increment float64) {
	drone.incrementFactor = increment
}

func (drone *Drone) EnableDebug() {
	drone.debug_flag = true
}

func (drone *Drone) GetEntityType() core.EntityType {
	return core.RealTime
}

/*
Create an Instance of a Drone with Randomized Movement

	Starting Coordinates : Lat:-33.946765,Lng:151.1796423

	Movement Pattern : Randomized

	Speed : 0.001 deg (111.319 met/s)
*/
func DefaultDrone() *Drone {
	d := &Drone{
		Config:           &DronePubSubConfig{},
		startTime:        time.Now(),
		incrementFactor:  0.001,
		movementStrategy: RandomMovement{},
		currPos:          &geo.LatLng{Lat: -33.946765, Lng: 151.1796423},
		StartPosition:    &geo.LatLng{Lat: -33.946765, Lng: 151.1796423},
	}

	return d
}

func DefaultCircularDrone() *Drone {
	def_loc := geo.LatLng{Lat: -33.946765, Lng: 151.1796423}

	d := NewDrone(&def_loc)

	center := geo.LatLng{Lat: -33.94755661744177, Lng: 151.17470741271973}

	circular_config := NewCircularConfig(&def_loc, &center, 0.001)

	d.ChangePattern(&circular_config)

	return d
}

func (drone *Drone) GetDefaultChannel() string {
	return drone.pubsubChannel
}

func (drone *Drone) EnableLogging() *Drone {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")
	return drone
}

func (drone *Drone) SetPubSubChannel(channel string) *Drone {
	drone.pubsubChannel = channel
	return drone
}

func (drone *Drone) movementStrategyToStr() string {
	switch drone.pattern {
	case ZigZag:
		return "ZigZag"
	case Circular:
		return "Circular"
	case Straight:
		return "Straight"
	case Random:
		return "Random"
	default:
		return "GoTo"

	}
}

func (drone *Drone) LogStats() {
	drone.RLock()
	defer drone.RUnlock()

	totalTimeElapsed := time.Now().Sub(drone.startTime).Seconds()

	droneStats := map[string]string{
		"Distance Travelled": fmt.Sprintf("%.2f meters", drone.totalDistance),
		"Total Time Elapsed": fmt.Sprintf("%.2f seconds", totalTimeElapsed),
		"Average Speed":      fmt.Sprintf("%.2f mph", geo.ConvertMpsMph(drone.totalDistance/totalTimeElapsed)),
		"Current Position":   fmt.Sprintf("Latitude %.6f, Longitude %.6f", drone.currPos.GetLat(), drone.currPos.GetLng()),
	}

	logger.LogKVResults(logger.BOLD, logger.COOLBLUE, droneStats)
}
