package integration_tests

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"
	"time"

	"droneshield/apiserver"
	"droneshield/client"
	"droneshield/core"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/publisher"
	"droneshield/subscriber"
	"droneshield/utilities"

	"github.com/gorilla/websocket"
)

/*

HIGHLY DISCOURAGED FOR REAL USE CASES

Make sure redis is running in the bg

*/

func TestGenericEndToEnd(t *testing.T) {
	go launchGenericEndToEndApp()

	time.Sleep(2 * time.Second)

	serverURL := "ws://localhost:8081/"

	conn, _, err := Dialer.Dial(serverURL, nil)
	if err != nil {
		t.Fatalf("Could not connect to WebSocket server: %v", err)
	}
	defer conn.Close()

	testMessage := "hello"
	if err := conn.WriteMessage(websocket.TextMessage, []byte(testMessage)); err != nil {
		t.Fatalf("Could not send message to server: %v", err)
	}

	_, message, err := conn.ReadMessage()
	if err != nil {
		t.Fatalf("Could not read message from server: %v", err)
	}

	log.Printf("Received message from server: %s", message)

	var coords DroneCoordinates
	if err := json.Unmarshal(message, &coords); err != nil {
		t.Fatalf("Received message does not match expected JSON structure: %v", err)
	}

	if coords.Latitude == 0 || coords.Longitude == 0 {
		t.Errorf("Received coordinates are not valid: %+v", coords)
	}
}

func launchGenericEndToEndApp() {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")

	// utils to enable graceful shutdown and profiling

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	utilities.HandleOSCancellationGracefully(ctx, cancel)
	p, _ := utilities.NewProfiler("../../release/profile").Tracing().Memory().CPU().Optimize().Start()
	defer p.Stop()

	/* Application Logic */

	client_addr := "localhost:6379"

	svc_entity := drone.DefaultDrone().SetPubSubChannel("drone_coordinates")

	// Any Client that implements core.PubSubClient can be swapped in

	client, err := client.NewRedisPubSubClient(ctx, client_addr)
	if err != nil && !utilities.IsNil(client) {
		logger.LogError(fmt.Sprintf("Failed to Connect to Generic Redis Client. ERROR:%s", err))
	}
	pubBuilder := publisher.NewPublisherBuilder(client, svc_entity)
	publisher := pubBuilder.NewPublisher()
	go publisher.Publish(ctx)

	// Any Server that implements core/APIServer can be swapped in
	apiServer := apiserver.NewAPIServer(apiserver.ServerConfig{
		HTTPPort:      ":8080",
		WebSocketPort: ":8081",
	})

	subBuilder := subscriber.NewSubscriberBuilder(svc_entity, client)

	// WebSockets Route for drone commands
	apiServer.AddWebSocketRoute("/", func(conn *websocket.Conn, r *http.Request) {
		genericHandler(ctx, conn, client, svc_entity, subBuilder)
	})

	// HTTP Route
	apiServer.AddHTTPRoute("/api", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello from HTTP!"))
	})

	apiServer.RunBlocking(ctx)
}

func genericHandler(ctx context.Context,
	conn *websocket.Conn,

	client core.PubSubClient,
	droneInstance core.PubSubEntity,
	subscriberBuilder core.SubscriberBuilder,
) {
	/*   1. Each connection/consumer has their Subscription to the Redis Channel   */

	go func() {
		subscriber := subscriberBuilder.NewSubscriber()
		subscriber.Subscribe(ctx, conn)
	}()

	/*    2. Process any incoming messages on the socket from the connected Client  */

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Printf("WebSocket read error: %v", err)
			break
		}

		// Process the message using the entity
		response, err := droneInstance.HandleAPIRequest(message)
		if err != nil {
			log.Printf("Error processing request: %v", err)
			continue
		}

		log.Printf("Response %s", response)

	}
}

var Dialer = websocket.Dialer{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type DroneCoordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}
