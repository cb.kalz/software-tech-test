package integration_tests

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"
	"time"

	"droneshield/apiserver"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/publisher"
	"droneshield/subscriber"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

/*

HIGHLY DISCOURAGED FOR REAL USE CASES

Make sure redis is running in the bg

*/

func TestSimpleEndToEnd(t *testing.T) {
	go mockSimpleIntegrationTest()

	time.Sleep(2 * time.Second)

	serverURL := "ws://localhost:8081/"

	conn, _, err := SDialer.Dial(serverURL, nil)
	if err != nil {
		t.Fatalf("Could not connect to WebSocket server: %v", err)
	}
	defer conn.Close()

	testMessage := "hello"
	if err := conn.WriteMessage(websocket.TextMessage, []byte(testMessage)); err != nil {
		t.Fatalf("Could not send message to server: %v", err)
	}

	_, message, err := conn.ReadMessage()
	if err != nil {
		t.Fatalf("Could not read message from server: %v", err)
	}

	log.Printf("Received message from server: %s", message)

	var coords SDroneCoordinates
	if err := json.Unmarshal(message, &coords); err != nil {
		t.Fatalf("Received message does not match expected JSON structure: %v", err)
	}

	if coords.Latitude == 0 || coords.Longitude == 0 {
		t.Errorf("Received coordinates are not valid: %+v", coords)
	}
}

// Just an example, in reality we would use Proper Mocks using our Interfaces
func mockSimpleIntegrationTest() {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	redis_addr := "localhost:6379"
	redis_chan := "drone_coordinates"

	redis_client := publisher.ConnectFatal(ctx, redis_addr)

	droneInstance := drone.DefaultDrone()
	droneInstance.SetPubSubChannel(redis_chan)

	redisCoordsPublisher := publisher.NewRedisCoordinatePublisher(redis_client, redis_chan, droneInstance)

	/* 1. Drone initialized & Publishing Begins here */
	go redisCoordsPublisher.Publish(ctx)

	cfg := apiserver.ServerConfig{
		HTTPPort:      ":8080",
		WebSocketPort: ":8081",
	}
	apiServer := apiserver.NewAPIServer(cfg)

	// WebSocket route for drone commands
	apiServer.AddWebSocketRoute("/", func(conn *websocket.Conn, r *http.Request) {
		droneTrackerPubSubHandler(ctx, conn, redis_client, redis_chan, droneInstance)
	})

	// HTTP route for drone commands
	apiServer.AddHTTPRoute("/drone", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		var cmd drone.Command
		if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Printf("Received HTTP Command: %+v", cmd)

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(map[string]string{"status": "success"})

		logger.LogEventCoded(logger.GREY_BLUE, "Received Command:", cmd, "Params:", cmd.Params)

		drone.UpdateDroneFromCmd(cmd, droneInstance)
	})

	apiServer.RunBlocking(ctx)
}

var SDialer = websocket.Dialer{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type SDroneCoordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func droneTrackerPubSubHandler(ctx context.Context,
	conn *websocket.Conn,
	client *redis.Client,
	channel string,
	droneInstance *drone.Drone,
) {
	/*   1. Each connection/consumer has their Subscription to the Redis Channel   */

	go func() {
		subscriber := subscriber.NewRedisChannelSubscriber(client)
		subscriber.Subscribe(ctx, conn, channel)
	}()

	/*    2. Process any incoming messages on the socket from the connected Client  */

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			// log.Printf("WebSocket read error: %v", err)
			break
		}

		var cmd drone.Command

		if err := json.Unmarshal(message, &cmd); err != nil {
			logger.LogError(fmt.Sprintf("Error decoding command: %v", err))
			continue
		}

		logger.LogEventCoded(logger.SAND, "Received Command:", cmd, "Params:", cmd.Params)

		drone.UpdateDroneFromCmd(cmd, droneInstance)

	}
}
