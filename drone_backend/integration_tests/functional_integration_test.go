package integration_tests

import (
	"encoding/json"
	"log"
	"testing"
	"time"

	"droneshield/apiserver"
	"droneshield/drone"
	"droneshield/logger"

	"github.com/gorilla/websocket"
)

/*

HIGHLY DISCOURAGED FOR REAL USE CASES

Make sure redis is running in the bg

*/

func TestFunctionalEndToEnd(t *testing.T) {
	go launchFunctionFullApp()

	time.Sleep(2 * time.Second)

	serverURL := "ws://localhost:8081/"

	conn, _, err := FDialer.Dial(serverURL, nil)
	if err != nil {
		t.Fatalf("Could not connect to WebSocket server: %v", err)
	}
	defer conn.Close()

	testMessage := "hello"
	if err := conn.WriteMessage(websocket.TextMessage, []byte(testMessage)); err != nil {
		t.Fatalf("Could not send message to server: %v", err)
	}

	_, message, err := conn.ReadMessage()
	if err != nil {
		t.Fatalf("Could not read message from server: %v", err)
	}

	log.Printf("Received message from server: %s", message)

	var coords FDroneCoordinates
	if err := json.Unmarshal(message, &coords); err != nil {
		t.Fatalf("Received message does not match expected JSON structure: %v", err)
	}

	if coords.Latitude == 0 || coords.Longitude == 0 {
		t.Errorf("Received coordinates are not valid: %+v", coords)
	}
}

func launchFunctionFullApp() {
	_, err := drone.DefaultDrone().
		WithChannel("drone_coordinates").
		EnableLogging().
		WithPubSub(":6379").
		WithAPIServer(apiserver.ServerConfig{
			HTTPPort:      "8080",
			WebSocketPort: "8081",
		}).
		LaunchPubSub(nil)
	if err != nil {
		logger.LogError("Error Launching DroneShield PubSub Service")
	}

	logger.LogSuccess("Pub Sub Service exiting")
}

var FDialer = websocket.Dialer{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type FDroneCoordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}
