package tests

import (
	"testing"

	"droneshield/geo"
)

func TestIsLocationValid(t *testing.T) {
	testCases := []struct {
		name     string
		location geo.LatLng
		expected bool
	}{
		{
			name:     "Valid Location",
			location: geo.LatLng{Lat: 30.0, Lng: 60.0},
			expected: true,
		},
		{
			name:     "Invalid Latitude",
			location: geo.LatLng{Lat: 190.0, Lng: 60.0},
			expected: false,
		},
		{
			name:     "Invalid Longitude",
			location: geo.LatLng{Lat: 30.0, Lng: 190.0},
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result := tc.location.IsLocationValid()

			if result != tc.expected {
				t.Errorf("Expected %v, but got %v", tc.expected, result)
			}
		})
	}
}

/*
# Compile with debug info to use lldb

go build -gcflags="all=-N -l" -o main

lldb main

breakpoint set --name main.main

run

n
n
s


*/
