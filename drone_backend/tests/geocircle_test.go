package tests

import (
	"math"
	"testing"

	"droneshield/geo"
)

// Test Outcome : Generate Coordinates starting from start_point : That are coords around the Circumference of the Circle
// Given a Start Position (x1,x2) and Pos of Center (x2,y2)
// Calculate the next Coordinate such that (x0,y0...xN,yN) is generated
// to travel around the Circumference of the circle with center (x2,y2) w/ R dist(startCircle,endCircle)

func TestGeoCircleRadius(t *testing.T) {
	start_point := geo.LatLng{Lat: -33.946765, Lng: 151.1796423}

	center_point := geo.LatLng{Lat: -33.94755661744177, Lng: 151.17470741271973}

	radius_meters := geo.CalculateDistanceMeters(&start_point, &center_point)

	if radius_meters < 10 {
		t.Errorf("Radius too small")
	}
}

func TestAngularInc(t *testing.T) {
	start_point := geo.LatLng{Lat: -33.946765, Lng: 151.1796423}
	center_point := geo.LatLng{Lat: -33.94755661744177, Lng: 151.17470741271973}

	radius_meters := geo.CalculateDistanceMeters(&start_point, &center_point)

	distance_per_update := 111.0

	dist_update_func := geo.DegreesToMeters(0.001)

	if dist_update_func != 111.319000 {
		t.Errorf("Degrees to Meters Failure for 0.001: %f", dist_update_func)
	}

	circumference := 2 * math.Pi * radius_meters
	angular_increment := distance_per_update / radius_meters

	if circumference < 10 {
		t.Errorf("Circumf too small")
	}
	if math.Abs(radius_meters-463.636822) > 1 {
		t.Errorf("Radius meters failure %f ", radius_meters)
	}

	if math.Abs(angular_increment-0.239412) > 0.01 {
		t.Errorf("Failed Angular Increment %f", angular_increment)
	}

	if math.Abs(circumference-2913.116071) > 10 {
		t.Errorf("Circumference Compute Failure %f", circumference)
	}
}
