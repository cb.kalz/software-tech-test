#!/bin/bash

ws_command='{"action": "changePattern","pattern": "ZigZag","params": {"target": {"latitude": -33.93755661744177,"longitude": 151.17470741271973},"angle": 45.0}}'

http_command='{"action": "changePattern", "pattern": "Circular", "params": {"center": {"latitude": -33.94755661744177, "longitude": 151.17470741271973}}}'


echo $ws_command | websocat ws://localhost:8080/ &
echo $ws_command | websocat ws://localhost:8080/ &

curl -X POST http://localhost:8081/drone -H "Content-Type: application/json" -d "$http_command" &
curl -X POST http://localhost:8081/drone -H "Content-Type: application/json" -d "$http_command" &

wait
