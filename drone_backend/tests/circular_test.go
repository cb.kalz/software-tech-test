package tests

import (
	"math"
	"testing"

	"droneshield/geo"
	"droneshield/measurement"
)

func TestCircleCircumferenceMethods(t *testing.T) {
	pos := geo.NewLatLng(-33.946765, 151.1796423)
	center := geo.NewLatLng(-33.94755661744177, 151.17470741271973)
	inc := measurement.Degrees(0.001)

	c_meters := pos.GetCircleCircumference(center)
	c_degrees := c_meters.ToDegrees().Float()
	req_increments_met := c_meters.Float() / inc.ToMeters().Float()
	req_increments_deg := c_degrees / inc.Float()
	diff := math.Abs(req_increments_deg - req_increments_met)

	if c_meters < 1 {
		t.Errorf("Geo Interface Metric Circumference Failure")
	}
	if c_degrees > 1 {
		t.Errorf("Geo Interface Degree Circumference Failure")
	}

	if req_increments_met > 28 || req_increments_met < 24 {
		t.Errorf("Measurement Interface Degree & Metric Conversion Failure")
	}
	if req_increments_deg > 28 || req_increments_deg < 24 {
		t.Errorf("Measurement Interface Degree & Metric Conversion Failure")
	}

	if diff > 1 {
		t.Errorf("Diff should be smaller than 1. Diff %f", diff)
		t.Errorf("Increment Calculation Failure. Circumference in Degrees: %f Increment: %f Requried Incs: %f", c_degrees, inc, req_increments_deg)
	}
}
