package tests

import (
	"math"
	"testing"

	"droneshield/geo"
)

func TestZigZagStepOneAngle(t *testing.T) {
	start := geo.LatLng{Lat: -33.92057152867508, Lng: 151.1723899841309}

	dest := geo.LatLng{Lat: -33.974541706208456, Lng: 151.1770248413086}

	bearing := start.CalculateBearing(&dest)

	if math.Abs(bearing-175.9264205421814) > 1 {
		t.Errorf("Angle bearing calculation off by < 1")
	}
}

func TestZigPartMovement(t *testing.T) {
	// input : start, destination, increment factor, angle for Diagonal
	start := geo.LatLng{Lat: -33.92057152867508, Lng: 151.1723899841309}
	dest := geo.LatLng{Lat: -33.974541706208456, Lng: 151.1770248413086}

	diag_angle := 45.0
	increment_factor := 0.001
	margin := increment_factor*2 + 0.05
	// calculate : Dist (degrees) and steps required

	distanceDegrees := start.DegreesBetween(&dest)
	steps_required := distanceDegrees / increment_factor

	// calculate bearing factor
	bearing := start.CalculateBearing(&dest)
	bearingRad := bearing * math.Pi / 180

	for i := 0; i < int(math.Round(steps_required)); i++ {
		start.IncrementLatLng(increment_factor*math.Cos(bearingRad), increment_factor*math.Sin(bearingRad))
	}

	if start.DegreesBetween(&dest) > increment_factor {
		t.Errorf("Distance off by greater than Increment after moving n steps")
	}

	// Calculate the diagonal (zag) movement angle based on the initial bearing plus diag_angle
	diagBearingRad := math.Mod(bearingRad+(diag_angle*math.Pi/180), 2*math.Pi)

	diag_movement_req := distanceDegrees * math.Sqrt(2)
	diag_steps_required := diag_movement_req / increment_factor

	// Perform the diagonal (zag) movement.
	for i := 0; i < int(math.Round(diag_steps_required)); i++ {
		start.IncrementLatLng(increment_factor*math.Cos(diagBearingRad), increment_factor*math.Sin(diagBearingRad))
	}

	finalDistFromDest := start.DegreesBetween(&dest)

	if math.Abs(finalDistFromDest-diag_movement_req) > margin {
		t.Errorf("Drone did not complete the expected diagonal (zag) movement. Distance moved: %f, Expected: %f", finalDistFromDest, diag_movement_req)
	}
}
