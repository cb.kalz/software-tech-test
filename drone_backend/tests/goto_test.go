package tests

import (
	"math"
	"testing"

	"droneshield/geo"
	"droneshield/measurement"
)

func TestGoToMovement(t *testing.T) {
	start := geo.LatLng{Lat: -33.92057152867508, Lng: 151.1723899841309}
	dest := geo.LatLng{Lat: -33.974541706208456, Lng: 151.1770248413086}

	increment_factor := 0.001
	distanceDegrees := start.DegreesBetween(&dest)
	steps_required := distanceDegrees / increment_factor

	bearing := start.CalculateBearing(&dest)
	bearingRad := bearing * math.Pi / 180

	for i := 0; i < int(math.Round(steps_required)); i++ {
		start.IncrementLatLng(increment_factor*math.Cos(bearingRad), increment_factor*math.Sin(bearingRad))
	}

	metersThreshold := measurement.Degrees(increment_factor).ToMeters().Float()
	degreesThreshold := increment_factor

	if withinMeters := start.DistanceToPointMetersWithinThreshold(&dest, metersThreshold); !withinMeters {
		t.Errorf("Ending position is not within %v meters of the destination", metersThreshold)
	}

	if withinDegrees, _ := start.DistanceToPointDegreesWithinThreshold(&dest, degreesThreshold); !withinDegrees {
		t.Errorf("Ending position is not within threshold of %v degrees of the destination", degreesThreshold)
	}
}

// go test
// go test circle_test.go
