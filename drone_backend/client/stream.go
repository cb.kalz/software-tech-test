package client

import (
	"droneshield/core"

	"github.com/go-redis/redis/v8"
)

type RedisPubSubStreamWrapper struct {
	pubSub *redis.PubSub
}

func NewRedisPubSubStreamWrapper(pubSub *redis.PubSub) *RedisPubSubStreamWrapper {
	return &RedisPubSubStreamWrapper{pubSub: pubSub}
}

func (wrapper *RedisPubSubStreamWrapper) Close() error {
	return wrapper.pubSub.Close()
}

func (wrapper *RedisPubSubStreamWrapper) Channel() <-chan core.PubSubMessage {
	output := make(chan core.PubSubMessage)
	go func() {
		for msg := range wrapper.pubSub.Channel() {
			output <- NewRedisPubSubMessageWrapper(msg)
		}
		close(output)
	}()
	return output
}

type RedisPubSubMessageWrapper struct {
	msg *redis.Message
}

func NewRedisPubSubMessageWrapper(msg *redis.Message) *RedisPubSubMessageWrapper {
	return &RedisPubSubMessageWrapper{msg: msg}
}

func (m *RedisPubSubMessageWrapper) Payload() []byte {
	return []byte(m.msg.Payload)
}
