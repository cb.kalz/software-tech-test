package client

import (
	"context"
	"fmt"

	"droneshield/core"
	"droneshield/logger"
)

/*
Creates a Redis Client to be used in a Pub/Sub Service contexts
*/
func NewRedisPubSubClient(ctx context.Context, addr string) (core.PubSubClient, error) {
	client, err := NewRedisClient(ctx, addr)
	if err != nil {
		logger.LogError("Failure Connecting to Client")
		return nil, err
	}
	if client == nil {
		logger.LogError("Nil Redis Client Returned, Failure")
		return nil, fmt.Errorf("Nil Redis Client Returned")
	}

	return client, nil
}
