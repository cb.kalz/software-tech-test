package client

import (
	"context"
	"errors"
	"fmt"
	"log"

	"droneshield/core"

	"github.com/go-redis/redis/v8"
)

/*

For Core Applications and Libraries - we can take this approach.

There are endless positives/benefits to this and it really depends on the team and engineers which way they prefer.

We essentially create a Generic "Client" for usage with Pub/Sub Applications.

The main benefits that can be a justification for the
Added Complexity and Pitfalls introduced can be (in my opinion) :

1. Velocity & Quality

This in my opinion is the biggest benefit - when multiple engineers are on the same page already
& they are working on a feature together - and the end result should be of High Quality.

This approach enables the team to say, work on the same codebase at the same time and explore
patterns and develop while ensuring there is no Quality Compromise.

2. Consistent Usage.
For example if we onboard 3 new junior engineers - and we want them to work on some features
using our Production Redis.

Do we want to give access to the Prod Redis instance and let engs call any potential methods from the Library?

Probably not - so we can use an interface driven approach and use Composition + Interfaces to provide an interface.
*/

type RedisClient struct {
	addr        string
	redisClient *redis.Client
}

func NewRedisClient(ctx context.Context, addr string) (*RedisClient, error) {
	redisClient := RedisClient{addr: addr}

	err := redisClient.Connect(ctx)
	if err != nil {
		return nil, err
	}

	log.Printf("Created New Redis Client")

	return &redisClient, nil
}

func (r *RedisClient) Connect(ctx context.Context) error {
	client, err := Connect(ctx, r.addr)
	if err != nil {
		return errors.New(fmt.Sprintf("Failed Connection to Redis Client at %s", r.addr))
	}

	if client == nil {
		return errors.New(fmt.Sprintf("Client returned as Nil Pointer to Redis Client at %s", r.addr))
	}

	r.redisClient = client
	return nil
}

func (r *RedisClient) Publish(ctx context.Context, channel string, data []byte) error {
	return r.redisClient.Publish(ctx, channel, data).Err()
}

func (r *RedisClient) Ping(ctx context.Context) error {
	_, err := r.redisClient.Ping(ctx).Result()
	return err
}

func (r *RedisClient) Subscribe(ctx context.Context, channel string) core.PubSubStream {
	pubSub := r.redisClient.Subscribe(ctx, channel)
	return NewRedisPubSubStreamWrapper(pubSub)
}

func (r *RedisClient) GetClientType() []core.ClientType {
	return []core.ClientType{core.HTTP, core.InMemory}
}
