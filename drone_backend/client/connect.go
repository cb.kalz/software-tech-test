package client

import (
	"context"
	"log"

	"droneshield/logger"

	"github.com/go-redis/redis/v8"
)

func Connect(ctx context.Context, addr string) (*redis.Client, error) {
	log.Printf("Connecting to Redis at %s%s%s", logger.PEACH, addr, logger.NC)
	rdb := redis.NewClient(&redis.Options{
		Addr: addr,
	})
	log.Printf("Created Redis Client. Checking Connection Health")
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		log.Printf("Error connecting to Redis: %s", err)
		return nil, err
	}

	logger.LogSuccess("Redis Client Successfully Established!")

	return rdb, nil
}
