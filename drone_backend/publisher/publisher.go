package publisher

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"droneshield/core"
	"droneshield/logger"
)

/* Extensible Interface impl to use for Producer Services */

type PublisherService struct {
	client  core.PubSubClient
	channel string
	entity  core.PubSubEntity
}

/*
Returns a Publisher Builder that Implements Interface for core/PublisherService

	type PublisherService interface {
		NewPublisher() Publisher
	}

This enables us to write and iterate on Maintainable Logic
Notice that in the PublisherService we did not include a Client and an Entity in the Method Signature

Because we might have a Producer that required 2 clients - or 3 entities as an example
*/
func NewPublisherBuilder(client core.PubSubClient, entity core.PubSubEntity) PublisherService {
	return PublisherService{
		client:  client,
		channel: entity.GetDefaultChannel(),
		entity:  entity,
	}
}

/*
Creates a Publisher that Implements the Interface for core/Publisher

	type Publisher interface {
		Service
		Publish(context.Context)
		PingClient(context.Context) (bool, error)
	}

This allows us to create any Publisher and use it with our codebase
*/
func (svc PublisherService) NewPublisher() core.Publisher {
	return NewPublisher(svc.client, svc.channel, svc.entity)
}

type GenPublisher struct {
	client  core.PubSubClient
	channel string
	entity  core.PubSubEntity
}

func NewPublisher(client core.PubSubClient, channel string, entity core.PubSubEntity) *GenPublisher {
	log.Printf("Launching Redis Publisher")

	return &GenPublisher{
		client:  client,
		channel: channel,
		entity:  entity,
	}
}

/*
Publishes the Serialized Event received from PubSubEntity
*/
func (rp *GenPublisher) Publish(ctx context.Context) {
	logger.LogSectionBlueAlt("Publisher Running")

	for {
		select {
		case <-ctx.Done():
			logger.LogErrorDim("Stopping Redis publisher")
			return
		default:
			data, err := rp.entity.PublishSerializedEvent()
			if err != nil {
				logger.LogError(fmt.Sprintf("Failed to Publish Serialized Event ERROR: %s", err))
				os.Exit(1)
			}
			log.Printf("%sPublishing Event to Channel%s: %s", logger.LIGHT_GREY, logger.NC, string(data))

			err = rp.client.Publish(ctx, rp.channel, data)
			if err != nil {
				fmt.Println("Error publishing drone coordinates:", err)
			}
			time.Sleep(time.Second)
		}
	}
}

func (rp *GenPublisher) PingClient(ctx context.Context) (bool, error) {
	err := rp.client.Ping(ctx)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (p GenPublisher) GetServiceType() core.ServiceType {
	return core.PublisherSvc
}
