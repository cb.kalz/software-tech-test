package publisher

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"droneshield/core"
	"droneshield/logger"

	"github.com/go-redis/redis/v8"
)

// to create the Publisher in the Main Svc using Interface method
// NewPublisher(client Client, entity Entity, channel string) Publisher
type RedisPublisherSvc struct{}

/* Satisfies the core/Publisher interface */

type RedisCoordinatePublisher struct {
	client    *redis.Client
	channel   string
	publisher core.CoordinatePublisher
}

/*
	Drone Redis Publisher that emits Coordinates to a Redis Channel specified for Consumers to Poll/Subscribe to

Usage:

	  redis_client   := ConnectRedisFatal(ctx, ":6379")
		redisPublisher := NewRedisCoordinatePublisher(redis_client, "drone_coords", droneInstance)
*/

func NewRedisCoordinatePublisher(client *redis.Client, channel string, publisher core.CoordinatePublisher) *RedisCoordinatePublisher {
	log.Printf("Launching Redis Publisher")

	return &RedisCoordinatePublisher{
		client:    client,
		channel:   channel,
		publisher: publisher,
	}
}

/*
Starts Publishing Coordinates to the Redis Channel Specified

Usage:

	// Launch the Publisher in a GoRoutine
	// Coordinates will be written to the redis channel

	go redisCoordsPublisher.Publish(ctx)
*/
func (rp *RedisCoordinatePublisher) Publish(ctx context.Context) {
	time.Sleep(1 * time.Second)
	logger.LogSectionBlueAlt("Publisher Running")

	for {
		select {
		case <-ctx.Done():
			logger.LogWarning("Stopping Redis publisher")
			return
		default:
			coordinates := rp.publisher.EmitCoords()
			log.Printf("%sPublishing Event to Channel%s: %v", logger.LIGHT_GREY, logger.NC, coordinates)

			data, _ := json.Marshal(coordinates)
			err := rp.client.Publish(ctx, rp.channel, data).Err()
			if err != nil {
				fmt.Println("Error publishing drone coordinates:", err)
			}
			time.Sleep(time.Second)
		}
	}
}

// Call Method when the application must not proceed without a valid Redis Client
func ConnectFatal(ctx context.Context, addr string) *redis.Client {
	rdb, err := Connect(ctx, addr)
	if err != nil {
		log.Fatalf("Failed to connect to Redis: %v", err)
	}
	return rdb
}

func Connect(ctx context.Context, addr string) (*redis.Client, error) {
	log.Printf("Connecting to Redis at %s%s%s", logger.PEACH, addr, logger.NC)
	rdb := redis.NewClient(&redis.Options{
		Addr: addr,
	})
	log.Printf("Created Redis Client. Checking Connection Health")
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		log.Printf("Error connecting to Redis: %s", err)
		return nil, err
	}

	logger.LogSuccess("Redis Connection Successfully Established!")

	return rdb, nil
}

func (p RedisCoordinatePublisher) GetServiceType() core.ServiceType {
	return core.PublisherSvc
}
