#!/bin/bash


PPROF_BASE_URL="http://localhost:6060/debug/pprof"

echo -e "Run the application and kick off shell script to gather and visualize profiles"

echo -e "application/generic/main.go and application/functional/main.go include the Runtime profiling, and that should be the recommended approach"

capture_cpu_profile() {
    duration=$1
    echo "Capturing CPU profile for ${duration} seconds..."
    go tool pprof -pdf "${PPROF_BASE_URL}/profile?seconds=${duration}" > "cpu_profile_${duration}s.pdf"
}


capture_heap_profile() {
    echo "Capturing heap profile..."
    go tool pprof -pdf "${PPROF_BASE_URL}/heap" > heap_profile.pdf
}


capture_block_profile() {
    echo "Capturing block profile..."
    go tool pprof -pdf "${PPROF_BASE_URL}/block" > block_profile.pdf
}


capture_goroutine_profile() {
    echo "Capturing goroutine profile..."
    go tool pprof -pdf "${PPROF_BASE_URL}/goroutine" > goroutine_profile.pdf
}


capture_cpu_profile 120  # 2 minutes
capture_cpu_profile 300  # 5 minutes
capture_cpu_profile 600  # 10 minutes
capture_heap_profile
capture_block_profile
capture_goroutine_profile

echo "Profile capture completed."
