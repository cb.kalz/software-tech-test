package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"droneshield/apiserver"
	"droneshield/client"
	"droneshield/core"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/publisher"
	"droneshield/subscriber"
	"droneshield/utilities"

	"github.com/gorilla/websocket"
)

/*
Extensible Implementation:

Composition + Interfaces Driven Design

Interfaces defined in package droneshield/core
Our "Publishers" , "Subscribers" , "Clients" , and "Entities" need to implement Interfaces

I believe this is a good balance of Abstraction as it involves -

  - Creating a Specialized Implementation depending on Externals like DB's, Caches, etc.

  - Allows us to develop logic to reuse these implementations using different Entities

  - We might want to run the same app using a Radar for example
*/
func main() {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")

	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		logger.LogError("REDIS_PORT environment variable not set. Please make sure the Docker Compose, Deployment yaml, or environment specifies where Redis is Running")
		os.Exit(1)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	/* utils to enable graceful shutdown and profiling */
	utilities.HandleOSCancellationGracefully(ctx, cancel)
	p, _ := utilities.NewProfiler("../../release/profile").Tracing().Memory().CPU().Optimize().Start()
	defer p.Stop()

	/* Application Logic */

	svc_entity := drone.DefaultDrone().SetPubSubChannel("drone_coordinates")

	// Any Client that implements core.PubSubClient can be swapped in

	client, err := client.NewRedisPubSubClient(ctx, redisAddr)
	if err != nil && !utilities.IsNil(client) {
		logger.LogError(fmt.Sprintf("Failed to Connect to Generic Redis Client. ERROR:%s", err))
	}
	pubBuilder := publisher.NewPublisherBuilder(client, svc_entity)
	publisher := pubBuilder.NewPublisher()
	go publisher.Publish(ctx)

	// Any Server that implements core/APIServer can be swapped in
	apiServer := apiserver.NewAPIServer(apiserver.ServerConfig{
		HTTPPort:      ":8080",
		WebSocketPort: ":8081",
	})

	subBuilder := subscriber.NewSubscriberBuilder(svc_entity, client)

	// WebSockets Route for drone commands
	apiServer.AddWebSocketRoute("/", func(conn *websocket.Conn, r *http.Request) {
		genericHandler(ctx, conn, client, svc_entity, subBuilder)
	})

	// HTTP Route
	apiServer.AddHTTPRoute("/api", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello from HTTP!"))
	})

	apiServer.RunBlocking(ctx)
	svc_entity.LogStats()
}

func genericHandler(ctx context.Context,
	conn *websocket.Conn,

	client core.PubSubClient, // we might want to pass a Postgres Client or Kafka Client for example
	droneInstance core.PubSubEntity, // we might want to use the service with a Radar instead of a Drone for ex
	subscriberBuilder core.SubscriberBuilder,
) {
	/*   1. Each connection/consumer has their Subscription to the Redis Channel   */

	go func() {
		subscriber := subscriberBuilder.NewSubscriber()
		subscriber.Subscribe(ctx, conn)
	}()

	/*    2. Process any incoming messages on the socket from the connected Client  */

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Printf("WebSocket read error: %v", err)
			break
		}

		// Our Entity implements the core/PubSubEntity Serializeable interface
		// So any new entity we add we simply need to make sure to Implement the same methods & we can swap it in

		response, err := droneInstance.HandleAPIRequest(message)
		if err != nil {
			log.Printf("Error processing request: %v", err)
			continue
		}

		log.Printf("Response %s", response)

	}
}
