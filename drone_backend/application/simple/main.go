package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"droneshield/apiserver"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/publisher"
	"droneshield/subscriber"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

const (
	exitCodeErr       = 1
	exitCodeInterrupt = 2
)

func main() {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		logger.LogError("REDIS_PORT environment variable not set. Please make sure the Docker Compose, Deployment yaml, or environment specifies where Redis is Running")
		os.Exit(1)
	}

	redisChan := "drone_coordinates"

	client := publisher.ConnectFatal(ctx, redisAddr)

	droneInstance := drone.DefaultDrone()
	droneInstance.SetPubSubChannel(redisChan)

	redisCoordsPublisher := publisher.NewRedisCoordinatePublisher(client, redisChan, droneInstance)

	/* 1. Drone initialized & Publishing Begins here */
	go redisCoordsPublisher.Publish(ctx)

	cfg := apiserver.ServerConfig{
		HTTPPort:      ":8080",
		WebSocketPort: ":8082",
	}
	apiServer := apiserver.NewAPIServer(cfg)

	// WebSocket route for drone commands
	apiServer.AddWebSocketRoute("/", func(conn *websocket.Conn, r *http.Request) {
		droneTrackerPubSubHandler(ctx, conn, client, redisChan, droneInstance)
	})

	// HTTP route for drone commands
	apiServer.AddHTTPRoute("/drone", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		var cmd drone.Command
		if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Printf("Received HTTP Command: %+v", cmd)

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(map[string]string{"status": "success"})

		logger.LogEventCoded(logger.GREY_BLUE, "Received Command:", cmd, "Params:", cmd.Params)

		drone.UpdateDroneFromCmd(cmd, droneInstance)
	})

	apiServer.RunBlocking(ctx)
}

func droneTrackerPubSubHandler(ctx context.Context,
	conn *websocket.Conn,
	client *redis.Client,

	channel string,
	droneInstance *drone.Drone,
) {
	/*   1. Each connection/consumer has their Subscription to the Redis Channel   */

	go func() {
		subscriber := subscriber.NewRedisChannelSubscriber(client)
		subscriber.Subscribe(ctx, conn, channel)
	}()

	/*    2. Process any incoming messages on the socket from the connected Client  */

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			// log.Printf("WebSocket read error: %v", err)
			break
		}

		var cmd drone.Command

		if err := json.Unmarshal(message, &cmd); err != nil {
			logger.LogError(fmt.Sprintf("Error decoding command: %v", err))
			continue
		}

		logger.LogEventCoded(logger.SAND, "Received Command:", cmd, "Params:", cmd.Params)

		drone.UpdateDroneFromCmd(cmd, droneInstance)

	}
}

/* For new socket handlers - simply create a new Handler and Subscriber
that satisfies the WSocketRedisSubscribable interface */
