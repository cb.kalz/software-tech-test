package main

import (
	"context"
	"os"

	"droneshield/apiserver"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/utilities"
)

/*

* Functional + Composition Driven

This approach brings everything together for a clean functional entry point into the Application.

Depending on the use case - appropriate if we want to Tightly Couple our main Entity (Drone) with the business logic

I think of it as Tightly Coupled because - even though using Interfaces , Composition, etc. makes this Possible

Added complexity down the road and Refactoring are non-trivial aspects of going this Approach

The approach in application/simple can be easier for the Readers and teammates to follow - and easily Refactorable

*/

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		logger.LogError("REDIS_PORT environment variable not set. Please make sure the Docker Compose, Deployment yaml, or environment specifies where Redis is Running")
		os.Exit(1)
	}

	utilities.HandleOSCancellationGracefully(ctx, cancel)
	p, _ := utilities.NewProfiler("../../release/profile").Tracing().Memory().CPU().Optimize().Start()
	defer p.Stop()

	drone, err := drone.DefaultDrone().
		WithChannel("drone_coordinates").
		EnableLogging().
		WithPubSub(redisAddr).
		WithAPIServer(apiserver.ServerConfig{
			HTTPPort:      "8080",
			WebSocketPort: "8081",
		}).
		LaunchPubSub(ctx)
	if err != nil {
		logger.LogError("Error Launching DroneShield PubSub Service")
	}

	logger.LogSuccess("Pub Sub Service exiting")
	drone.LogStats()
}
