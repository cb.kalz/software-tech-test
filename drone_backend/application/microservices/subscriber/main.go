package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"droneshield/apiserver"
	"droneshield/client"
	"droneshield/core"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/subscriber"
	"droneshield/utilities"

	"github.com/gorilla/websocket"
)

var (
	redisAddr          string
	redisChan          = "drone_coordinates"
	stateUpdateChannel = "drone_state_updates"
	exitCodeErr        = 1
	exitCodeInterrupt  = 2
	profilePath        = "./profile"
)

func init() {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")

	redisAddr = os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		logger.LogError("REDIS_PORT environment variable not set. Please make sure the Docker Compose, Deployment yaml, or environment specifies where Redis is Running")
		os.Exit(1)
	}
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	utilities.HandleOSCancellationGracefully(ctx, cancel) // profiler, should use OTEL export
	p, _ := utilities.NewProfiler(profilePath).Tracing().Memory().CPU().Optimize().Start()
	defer p.Stop()

	client, err := client.NewRedisPubSubClient(ctx, redisAddr)
	if err != nil && !utilities.IsNil(client) {
		logger.LogError(fmt.Sprintf("Failed to Connect to Generic Redis Client. ERROR:%s", err))
	}
	droneInstance := drone.DefaultDrone().WithChannel(redisChan)

	// API Server that we can use to Run different types of Servers
	apiServer := apiserver.NewAPIServer(apiserver.ServerConfig{
		HTTPPort:      ":8080",
		WebSocketPort: ":8081",
	})

	/*
		Note that our API Server and Routes take core/PubSubEntity as a Param

		We did not hardcode the type for Drone - and relied on Interfaces + Composition
		This way, down the line we can easily swap out pieces as required!
	*/

	/* Websocket & HTTP Handlers */

	apiServer.AddWebSocketRoute("/", func(conn *websocket.Conn, r *http.Request) {
		websocketHandler(ctx, conn, client, droneInstance)
	})

	apiServer.AddHTTPRoute("/api", func(w http.ResponseWriter, r *http.Request) {
		httpHandler(ctx, w, r, client, droneInstance)
	})

	apiServer.RunBlocking(ctx)
}

/*
This design allows us to :
  - Swap the Redis Client with Postgres Client or Kafka Client
  - Use the same logic for a Radar instead of a Drone
*/
func websocketHandler(ctx context.Context,
	conn *websocket.Conn,
	client core.PubSubClient,
	droneInstance core.PubSubEntity,
) {
	/*   1. Each connection/consumer has their Subscription to the Redis Channel   */

	go func() {
		log.Printf("New Connection from %+v", conn.RemoteAddr())
		subBuilder := subscriber.NewSubscriberBuilder(droneInstance, client)
		subscriber := subBuilder.NewSubscriber()
		subscriber.Subscribe(ctx, conn)
	}()

	/*    2. Process any incoming messages on the socket from the connected Client  */

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Printf("WebSocket read error: %v", err)
			break
		}

		err = client.Publish(ctx, stateUpdateChannel, message)

		if err != nil {
			log.Printf("Error publishing command to Redis: %v", err)
			apiserver.WriteError(conn, fmt.Sprintf("Error dispatching command: %v", err))
		} else {
			apiserver.WriteSuccess(conn, "Successfully Dispatched Command for State Update")
		}

	}
}

/* Similarly , our Application Entities implement methods required for Responding to HTTP and WS Requests */
func httpHandler(ctx context.Context,
	w http.ResponseWriter,
	r *http.Request,
	client core.PubSubClient,
	droneInstance core.PubSubEntity,
) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading request body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = client.Publish(ctx, stateUpdateChannel, body)
	if err != nil {
		logger.LogError(fmt.Sprintf("Error publishing updated state to Redis: %v", err))
		return
	}

	logger.LogBold("Successfully Dispatched Command for state update")

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Command dispatched for state update"))
}
