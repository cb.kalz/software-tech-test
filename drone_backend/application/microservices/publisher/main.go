package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"droneshield/client"
	"droneshield/core"
	"droneshield/drone"
	"droneshield/logger"
	"droneshield/publisher"
	"droneshield/utilities"
)

/*
Publisher Microservice
We develop this using our Library using a cohesive shared codebase
Then we deploy the binary to its' own isolated VM/Container/Server Runtime.
*/

var (
	redisAddr          string
	stateUpdateChannel = "drone_state_updates"
	profilePath        = "./profile"
)

func init() {
	logger.EnableInfo()
	logger.LogMainAction("Launching Drone Emitter and Subscription Service")

	redisAddr = os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		logger.LogError("REDIS_PORT environment variable not set. Please make sure the Docker Compose, Deployment yaml, or environment specifies where Redis is Running")
		os.Exit(1)
	}
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	utilities.HandleOSCancellationGracefully(ctx, cancel) // profiler, should use OTEL export
	p, _ := utilities.NewProfiler(profilePath).Tracing().Memory().CPU().Optimize().Start()
	defer p.Stop()

	/* Application Logic

	1. Entity driving Pub/Sub Logic such as "What" to Publish & Data Serialization

	2. A Swappable Client we can use in any Pub/Sub Use Case

	3. Publisher that uses a Generic Service Entity & Client - to execute the Publishing

	4. Kick off Publishing Dynamically Asynchronously
				Update the Drone based on External Requests to a Redis Channel

	*/

	drone := drone.DefaultDrone().SetPubSubChannel("drone_coordinates")

	client, err := client.NewRedisPubSubClient(ctx, redisAddr)

	if err != nil && !utilities.IsNil(client) {
		logger.LogError(fmt.Sprintf("Failed to Connect to Generic Redis Client. ERROR:%s", err))
	}

	publisher := CreatePublisherFromEntityAndClient(client, drone)

	go BeginPublishing(ctx, publisher)

	go PollStateUpdatesChannel(ctx, client, drone)

	sigChan := make(chan os.Signal, 1) // block until a signal is received
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigChan
	log.Printf("Received signal %s, initiating shutdown.\n", sig)
	drone.LogStats()
	cancel()
	log.Printf("Application shutdown gracefully.")
}

/*
Creates a Publisher from:

- svcEntity implementing 		-> core.PubSubEntity

- pubSubClient implementing -> core.PubSubClient
*/
func CreatePublisherFromEntityAndClient(pubsubClient core.PubSubClient, svcEntity core.PubSubEntity) core.Publisher {
	pubBuilder := publisher.NewPublisherBuilder(pubsubClient, svcEntity)
	publisher := pubBuilder.NewPublisher()
	return publisher
}

// Runs the Publisher to kick off Publishing Dynamically
func BeginPublishing(ctx context.Context, publisher core.Publisher) {
	publisher.Publish(ctx)
}

/*
Listens to Asynchronous State Updates for a Decoupled Approach by Polling a Seperate Channel

This is a naive implementation as we would make sure there is an Expiry for Updates - such as 2000ms
So after 2000ms the channel is cleared - since we do not want to perform stale Updates
*/
func PollStateUpdatesChannel(ctx context.Context, pubsubClient core.PubSubClient, svcEntity core.PubSubEntity) {
	pubsub := pubsubClient.Subscribe(ctx, stateUpdateChannel)
	defer pubsub.Close()

	messageChannel := pubsub.Channel()

	for msg := range messageChannel {

		log.Printf("Received State Update")

		resp, err := svcEntity.HandleAPIRequest(msg.Payload())
		if err != nil {
			logger.LogError(fmt.Sprintf("Error Updating State from Microservice %s", err))
		}

		log.Printf("Resp %s", string(resp))
	}
}
