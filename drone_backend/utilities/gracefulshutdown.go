package utilities

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

/*
Interrupt OS Cancellation signals such as SIGINT , Shutdown from Ctrl+C , to exit gracefully

Optionally call this directly in main (preferred)

const (

	exitCodeErr       = 1
	exitCodeInterrupt = 2

)

	func main() {
		signalChan := make(chan os.Signal, 1)
		signal.Notify(signalChan, os.Interrupt)
		defer func() {
			signal.Stop(signalChan)
			cancel()
		}()
		go func() {
			select {
			case <-signalChan:
				cancel()
			case <-ctx.Done():
			}
			<-signalChan
			os.Exit(exitCodeInterrupt)
		}()
	}
*/
func HandleOSCancellationGracefully(ctx context.Context, cancelFunc context.CancelFunc) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-signalChan
		cancelFunc()
	}()
}
