package utilities

import (
	"log"
	"reflect"
)

func AreAllNonNil(args ...interface{}) bool {
	allNonNil := true
	for i, arg := range args {
		if IsNil(arg) {
			log.Printf("Argument at position %d is nil\n", i)
			allNonNil = false
		}
	}
	return allNonNil
}

/*
Nil Checks

- Arrays, Slices, Maps - nil is returned true only if they are unitialized

Zero values will return false for Nil

	func main() {
		var ptr *int
		fmt.Println(isNil(ptr)) // true

		var s *struct{}
		fmt.Println(isNil(s)) // true

		var i interface{}
		fmt.Println(isNil(i)) // true

		i = 42
		fmt.Println(isNil(i)) // false

		var slice []int
		fmt.Println(isNil(slice)) // true

		slice = make([]int, 0)
		fmt.Println(isNil(slice)) // false
	}
*/
func IsNil(i interface{}) bool {
	if i == nil {
		return true
	}

	val := reflect.ValueOf(i)
	switch val.Kind() {
	// Check for nil pointers, slices, maps, channels, functions, and interfaces
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice, reflect.Func, reflect.Interface:
		return val.IsNil()
	}
	return false
}
