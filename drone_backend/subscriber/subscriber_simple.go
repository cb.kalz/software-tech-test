package subscriber

import (
	"context"
	"errors"
	"fmt"
	"log"

	"droneshield/core"
	"droneshield/logger"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

// For Actual Use -> subscriber/subscriber.go

/* Satisfies the interface in core/WSocketRedisSubscribable */

type RedisChannelSubscriber struct {
	Client         *redis.Client
	DefaultChannel string
	channels       map[string]struct{}
}

/*
	Creates a new Subscriber that can be Subscribed/Polled

Usage:

	var client *redis.Client
	subscriber := subscriber.NewRedisChannelSubscriber(client)
	subscriber.Subscribe(ctx, conn, channel)
*/
func NewRedisChannelSubscriber(client *redis.Client) *RedisChannelSubscriber {
	return &RedisChannelSubscriber{
		Client:   client,
		channels: make(map[string]struct{}),
	}
}

/* Subscribe to the Channel and forward events to receivers connected through the Socket */
func (r *RedisChannelSubscriber) Subscribe(ctx context.Context, wsConn *websocket.Conn, channel string) {
	pubsub := r.Client.Subscribe(ctx, channel)
	//	pubsub := r.Client.Subscribe(ctx, r.DefaultChannel)
	defer pubsub.Close()

	ch := pubsub.Channel()
	for {
		select {
		case msg := <-ch:
			if err := wsConn.WriteMessage(websocket.TextMessage, []byte(msg.Payload)); err != nil {

				log.Printf("Error sending message to WebSocket client: %v", err)

				return
			}
		case <-ctx.Done():
			return
		}
	}
}

func (r *RedisChannelSubscriber) ListChannels() []string {
	var channels []string
	for channel := range r.channels {
		channels = append(channels, channel)
	}
	return channels
}

func (r *RedisChannelSubscriber) AddChannel(channel string) error {
	r.channels[channel] = struct{}{}
	return nil
}

func (r *RedisChannelSubscriber) SubscribeToChannel(ctx context.Context, channel string, wsConn *websocket.Conn) error {
	if _, exists := r.channels[channel]; exists {
		return errors.New("channel already subscribed")
	}

	pubsub := r.Client.Subscribe(ctx, channel)
	defer pubsub.Close()

	r.AddChannel(channel)

	ch := pubsub.Channel()
	for {
		select {
		case msg := <-ch:
			if err := wsConn.WriteMessage(websocket.TextMessage, []byte(msg.Payload)); err != nil {
				logger.LogErrorDim(fmt.Sprintf("Error sending message to WebSocket client: %v", err))

				log.Printf("Error sending message to WebSocket client: %v", err)

				return err
			}
		case <-ctx.Done():
			return nil
		}
	}
}

func (s RedisChannelSubscriber) GetServiceType() core.ServiceType {
	return core.SubscriberSvc
}
