package subscriber

import (
	"context"
	"log"

	"droneshield/core"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

// Prefer using -> subscriber/subscriber.go

/*

This Subscriber is Generic over the Entity such as the Drone or Object determining Logic

I believe this is a good balance of Abstraction as it involves -

	- Creating a Specialized Implementation depending on Externals like DB's, Caches, etc.

	- Allows us to develop logic to reuse these implementations using different Entities

	- We might want to run the same app using a Radar for example


*/

type GenSubscriberBuilder struct {
	entity core.PubSubEntity
	client *redis.Client
}

func NewGenSubscriberBuilder(entity core.PubSubEntity, client *redis.Client) *GenSubscriberBuilder {
	return &GenSubscriberBuilder{entity: entity, client: client}
}

func (gsub *GenSubscriberBuilder) BuildSubscriber() *GenSubscriber {
	return NewGenSubscriber(gsub.entity, gsub.client, gsub.entity.GetDefaultChannel())
}

func NewGenSubscriber(entity core.PubSubEntity, client *redis.Client, channel string) *GenSubscriber {
	return &GenSubscriber{
		Entity:  entity,
		Client:  client,
		channel: channel,
	}
}

type GenSubscriber struct {
	Entity  core.PubSubEntity
	Client  *redis.Client
	channel string
}

func (r *GenSubscriber) Subscribe(ctx context.Context, wsConn *websocket.Conn) {
	pubsub := r.Client.Subscribe(ctx, r.Entity.GetDefaultChannel())

	defer pubsub.Close()

	log.Printf("GenSubscriber Using Subscribing to Channnel Subscribe %s", r.Entity.GetDefaultChannel())

	ch := pubsub.Channel()
	for {
		select {
		case msg := <-ch:
			if err := wsConn.WriteMessage(websocket.TextMessage, []byte(msg.Payload)); err != nil {
				log.Printf("Error sending message to WebSocket client: %v", err)
				return
			}
		case <-ctx.Done():
			return
		}
	}
}

func (s GenSubscriber) GetServiceType() core.ServiceType {
	return core.SubscriberSvc
}
