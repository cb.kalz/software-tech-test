package subscriber

import (
	"context"
	"fmt"
	"log"

	"droneshield/core"
	"droneshield/logger"

	"github.com/gorilla/websocket"
)

type SubscriberBuilder struct {
	entity core.PubSubEntity
	client core.PubSubClient
}

func NewSubscriberBuilder(entity core.PubSubEntity, client core.PubSubClient) *SubscriberBuilder {
	return &SubscriberBuilder{entity: entity, client: client}
}

func (gsub *SubscriberBuilder) NewSubscriber() core.SocketSubscribable {
	return NewSubscriber(gsub.entity, gsub.client, gsub.entity.GetDefaultChannel())
}

func NewSubscriber(entity core.PubSubEntity, client core.PubSubClient, channel string) *Subscriber {
	return &Subscriber{
		Entity:  entity,
		Client:  client,
		channel: channel,
	}
}

type Subscriber struct {
	Entity  core.PubSubEntity
	Client  core.PubSubClient
	channel string
}

func (r *Subscriber) Subscribe(ctx context.Context, wsConn *websocket.Conn) error {
	pubsub := r.Client.Subscribe(ctx, r.Entity.GetDefaultChannel())

	defer pubsub.Close()

	log.Printf("Subscribed to Channnel  %s%s%s%s", logger.BOLD, logger.UNDERLINE, r.Entity.GetDefaultChannel(), logger.NC)

	ch := pubsub.Channel()
	for {
		select {
		case msg := <-ch:
			if err := wsConn.WriteMessage(websocket.TextMessage, []byte(msg.Payload())); err != nil {
				logger.LogErrorDim(fmt.Sprintf("Error sending message to WebSocket client: %v", err))

				log.Printf("Error sending message to WebSocket client: %v", err)
				return err
			}
		case <-ctx.Done():
			return nil
		}
	}
}

func (s Subscriber) GetServiceType() core.ServiceType {
	return core.SubscriberSvc
}
